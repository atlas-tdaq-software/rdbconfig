// this is -*- c++ -*-

#ifndef RDBCONFIG_RDBCONFIGOBJECT_H_
#define RDBCONFIG_RDBCONFIGOBJECT_H_

#include <iostream>

#include <config/ConfigObjectImpl.h>
#include <ipc/object.h>
#include <rdb/rdb.hh>

#include "rdbconfig/ClassInfo.h"

namespace rdbconfig {
  class rdb_server;
}

class ConfigurationImpl;
class RdbConfiguration;

struct RdbConfigObjectInfo
{
  std::string m_object_id;
  rdbconfig::ClassInfo * m_class_info;
};

struct RdbConfigObjectNames
{
  std::string m_object_id;
  std::string m_class_name;

  RdbConfigObjectNames(const rdb::RDBObject &obj) :
      m_object_id(static_cast<const char*>(obj.name)), m_class_name(static_cast<const char*>(obj.classid))
  {
    ;
  }
};

class RdbConfigObject : public ConfigObjectImpl {

  friend class ConfigurationImpl;
  friend class RdbConfiguration;
  friend std::ostream& operator<<(std::ostream& s, const RdbConfigObject * obj);


  public:

    RdbConfigObject(const RdbConfigObjectNames& obj, ConfigurationImpl * impl) noexcept :
      ConfigObjectImpl (impl, obj.m_object_id),
      m_server         (static_cast<RdbConfiguration *>(impl)->m_server),
      m_class_info     (static_cast<RdbConfiguration *>(impl)->m_class_info[obj.m_class_name]),
      m_sav_list       (0,0,nullptr,0),
      m_mav_list       (0,0,nullptr,0),
      m_rvlist         (0,0,nullptr,0)
        {
        }

    RdbConfigObject(const RdbConfigObjectInfo& info, ConfigurationImpl * impl) noexcept :
      ConfigObjectImpl (impl, info.m_object_id),
      m_server         (static_cast<RdbConfiguration *>(impl)->m_server),
      m_class_info     (*info.m_class_info),
      m_sav_list       (0,0,nullptr,0),
      m_mav_list       (0,0,nullptr,0),
      m_rvlist         (0,0,nullptr,0)
        {
        }

    virtual ~RdbConfigObject() noexcept;


  public:

    virtual const std::string contained_in() const;

    virtual void get(const std::string& name, bool&           value);
    virtual void get(const std::string& name, uint8_t&        value);
    virtual void get(const std::string& name, int8_t&         value);
    virtual void get(const std::string& name, uint16_t&       value);
    virtual void get(const std::string& name, int16_t&        value);
    virtual void get(const std::string& name, uint32_t&       value);
    virtual void get(const std::string& name, int32_t&        value);
    virtual void get(const std::string& name, uint64_t&       value);
    virtual void get(const std::string& name, int64_t&        value);
    virtual void get(const std::string& name, float&          value);
    virtual void get(const std::string& name, double&         value);
    virtual void get(const std::string& name, std::string&    value);
    virtual void get(const std::string& name, ConfigObject&   value);

    virtual void get(const std::string& name, std::vector<bool>&           value);
    virtual void get(const std::string& name, std::vector<uint8_t>&        value);
    virtual void get(const std::string& name, std::vector<int8_t>&         value);
    virtual void get(const std::string& name, std::vector<uint16_t>&       value);
    virtual void get(const std::string& name, std::vector<int16_t>&        value);
    virtual void get(const std::string& name, std::vector<uint32_t>&       value);
    virtual void get(const std::string& name, std::vector<int32_t>&        value);
    virtual void get(const std::string& name, std::vector<uint64_t>&       value);
    virtual void get(const std::string& name, std::vector<int64_t>&        value);
    virtual void get(const std::string& name, std::vector<float>&          value);
    virtual void get(const std::string& name, std::vector<double>&         value);
    virtual void get(const std::string& name, std::vector<std::string>&    value);
    virtual void get(const std::string& name, std::vector<ConfigObject>&   value);

    virtual bool rel(const std::string& name, std::vector<ConfigObject>& value);
    virtual void referenced_by(std::vector<ConfigObject>& value, const std::string& association, bool check_composite_only, unsigned long rlevel, const std::vector<std::string> * rclasses) const;


    virtual void set(const std::string& name, bool                value);
    virtual void set(const std::string& name, uint8_t             value);
    virtual void set(const std::string& name, int8_t              value);
    virtual void set(const std::string& name, uint16_t            value);
    virtual void set(const std::string& name, int16_t             value);
    virtual void set(const std::string& name, uint32_t            value);
    virtual void set(const std::string& name, int32_t             value);
    virtual void set(const std::string& name, uint64_t            value);
    virtual void set(const std::string& name, int64_t             value);
    virtual void set(const std::string& name, float               value);
    virtual void set(const std::string& name, double              value);
    virtual void set(const std::string& name, const std::string&  value);

    virtual void set_enum(const std::string& attribute, const std::string& value);
    virtual void set_date(const std::string& attribute, const std::string& value);
    virtual void set_time(const std::string& attribute, const std::string& value);

    virtual void set_class(const std::string& attribute, const std::string& value);

    virtual void set(const std::string& name, const std::vector<bool>&           value);
    virtual void set(const std::string& name, const std::vector<uint8_t>&        value);
    virtual void set(const std::string& name, const std::vector<int8_t>&         value);
    virtual void set(const std::string& name, const std::vector<uint16_t>&       value);
    virtual void set(const std::string& name, const std::vector<int16_t>&        value);
    virtual void set(const std::string& name, const std::vector<uint32_t>&       value);
    virtual void set(const std::string& name, const std::vector<int32_t>&        value);
    virtual void set(const std::string& name, const std::vector<uint64_t>&       value);
    virtual void set(const std::string& name, const std::vector<int64_t>&        value);
    virtual void set(const std::string& name, const std::vector<float>&          value);
    virtual void set(const std::string& name, const std::vector<double>&         value);
    virtual void set(const std::string& name, const std::vector<std::string>&    value);

    virtual void set_enum(const std::string& attribute, const std::vector<std::string>& value);
    virtual void set_date(const std::string& attribute, const std::vector<std::string>& value);
    virtual void set_time(const std::string& attribute, const std::vector<std::string>& value);

    virtual void set_class(const std::string& attribute, const std::vector<std::string>& value);

    virtual void set(const std::string& name, const ConfigObject*                     value, bool skip_non_null_check);
    virtual void set(const std::string& name, const std::vector<const ConfigObject*>& value, bool skip_non_null_check);

    virtual void move(const std::string& at);
    virtual void rename(const std::string& new_id);

    virtual void clear() noexcept;

    virtual void reset();

  private:

    void get_object_values();

    rdb::DataUnion& get_attribute_value(const std::string& name);
    rdb::DataListUnion& get_attribute_values(const std::string& name);
    const rdb::RDBObject * get_relation_object(const std::string& name);
    rdb::RDBObjectList& get_relation_objects(const std::string& name);

    void set_attribute_value(const std::string& name, const char * type, rdb::DataUnion * value, rdb::DataListUnion * values);
    void set_relationship_value(const std::string& name, const ConfigObject * value, const std::vector<const ConfigObject*> * values, bool skip_non_null_check);

    // required by template method insert_object
    void set(const RdbConfigObjectNames&)
      {
        ;
      }

    void set(const RdbConfigObjectInfo&)
      {
        ;
      }

  private:

    rdbconfig::rdb_server * m_server;
    rdbconfig::ClassInfo& m_class_info;

    rdb::RDBAttributeCompactValueList m_sav_list;   // single-value attributes
    rdb::RDBAttributeCompactValuesList m_mav_list;  // multi-values attributes
    rdb::RDBRelationshipCompactValueList m_rvlist;

    static unsigned long m_number_of_cget_object_values_calls;
    static unsigned long m_number_of_xget_object_values_calls;

};

std::ostream& operator<<(std::ostream&, const RdbConfigObject *);


#endif // RDBCONFIG_RDBCONFIGOBJECT_H_
