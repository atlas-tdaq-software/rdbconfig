#ifndef RDBCONFIG_CLASSINFO_H_
#define RDBCONFIG_CLASSINFO_H_

#include <string>
#include <rdb/rdb.hh>

class IPCPartition;

namespace rdbconfig {

  class ClassInfo {

    public:

      ClassInfo() : m_svan_list(0), m_mvan_list(0), m_rn_list(0), m_inited(false) { ; }
      ~ClassInfo();

      void init(rdb::RDBNameList& san, rdb::RDBNameList& man, rdb::RDBNameList& rn);

      unsigned long get_sva_pos(const std::string& name) const {
        return get_pos(m_svan_list, "single-value attribute", name);
      }

      unsigned long get_mva_pos(const std::string& name) const {
        return get_pos(m_mvan_list, "multi-values attribute", name);
      }

      unsigned long get_rel_pos(const std::string& name) const {
        return get_pos(m_rn_list, "relationship", name);
      }

      unsigned long get_pos(const std::string& name) const {
        return get_pos(m_rn_list, nullptr, name);
      }

      bool get_inited() const {return m_inited;}

    private:

      rdb::RDBNameList_var m_svan_list;   // names of single-value attributes
      rdb::RDBNameList_var m_mvan_list;   // names of multi-values attributes
      rdb::RDBNameList_var m_rn_list;     // names of relationships

      bool m_inited;

      unsigned long get_pos(const rdb::RDBNameList_var& where, const char * what, const std::string& name) const;


  };

}

#endif
