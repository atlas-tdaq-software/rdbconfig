#ifndef RDBCONFIG_RDBSERVER_H_
#define RDBCONFIG_RDBSERVER_H_

#include <atomic>
#include <chrono>
#include <mutex>
#include <string>

#include <rdb/rdb.hh>
#include <rdb/errors.h>

class IPCPartition;
class RDBWriterPingCB;

namespace rdbconfig {

  class rdb_server {

    public:

      /// \throw daq::rdb::LookupFailed
      rdb_server(const std::string& partition_name, const std::string& server_name);
      ~rdb_server();

      /// \throw daq::rdb::LookupFailed
      void lookup();
      void clean();

      bool is_valid() const;

      bool is_writer() const {return m_is_writer;}

      bool is_inited() const {return m_inited;}

      rdb::cursor_var& read_cursor() { return m_read_cursor; }

      rdb::writer_var& write_cursor() { return m_write_cursor; }
      rdb::RDBSessionId write_session() const { return m_session; }

      IPCPartition * partition() { return m_partition; }
      const std::string& name() { return m_server_name; }


    private:

      IPCPartition * m_partition;
      std::string m_server_name;

      bool m_inited;
      bool m_is_writer;

      rdb::cursor_var m_read_cursor;

      rdb::writer_var m_write_cursor;
      rdb::RDBSessionId m_session;
      RDBWriterPingCB * m_ping_cb;
      rdb::ProcessInfo m_proc_info;

      std::mutex m_efficiency_mutex;
      std::chrono::time_point<std::chrono::steady_clock> m_interval_tp;
      std::atomic<std::int64_t> m_number_of_calls;
  };

}

#endif
