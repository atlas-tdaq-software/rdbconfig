// this is -*- c++ -*-
#ifndef RDBCONFIG_RDBCONFIGURATION_H_
#define RDBCONFIG_RDBCONFIGURATION_H_

#include <atomic>
#include <list>
#include <map>
#include <string>

#include <tbb/queuing_rw_mutex.h>

#include <config/ConfigurationImpl.h>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <rdb/rdb_info.h>
#include <rdbconfig/ClassInfo.h>


namespace rdbconfig {
  class rdb_server;
}

class RdbConfigObject;

class RdbConfiguration : public ConfigurationImpl {

  friend class RdbConfigObject;
  friend struct RdbConfigurationNotify;

  public:

    RdbConfiguration(Configuration * db) noexcept;
    virtual ~RdbConfiguration();


  public:

    virtual bool test_object(const std::string& class_name, const std::string& name, unsigned long rlevel, const std::vector<std::string> * rclasses);
    virtual void get(const std::string& class_name, const std::string& name, ConfigObject& object, unsigned long rlevel, const std::vector<std::string> * rclasses);
    virtual void get(const std::string& class_name, std::vector<ConfigObject>& objects, const std::string& query, unsigned long rlevel, const std::vector<std::string> * rclasses);
    virtual void get(const ConfigObject& obj_from, const std::string& query, std::vector<ConfigObject>& objects, unsigned long rlevel, const std::vector<std::string> * rclasses);
    virtual daq::config::class_t * get(const std::string& class_name, bool direct_only);
    virtual void get_superclasses(config::fmap<config::fset>& schema);

    virtual void create(const std::string& at, const std::string& class_name, const std::string& id, ConfigObject& object) {
      create(&at, 0, class_name, id, object);
    }
    virtual void create(const ConfigObject& at, const std::string& class_name, const std::string& id, ConfigObject& object) {
      create(0, &at, class_name, id, object);
    }
    virtual void destroy(ConfigObject& object);

    virtual void open_db(const std::string& db_name);
    virtual void close_db();
    virtual bool loaded() const noexcept;
    virtual void create(const std::string& db_name, const std::list<std::string>& includes);
    virtual bool is_writable(const std::string& db_name);
    virtual void add_include(const std::string& db_name, const std::string& include);
    virtual void remove_include(const std::string& db_name, const std::string& include);
    virtual void get_includes(const std::string& db_name, std::list<std::string>& includes) const;
    virtual void get_updated_dbs(std::list<std::string>& dbs) const;
    virtual void set_commit_credentials(const std::string& user, const std::string& password);
    virtual void commit(const std::string& why);
    virtual void abort();
    virtual void prefetch_data();
    virtual void prefetch_schema();
    virtual std::vector<daq::config::Version> get_changes();
    virtual std::vector<daq::config::Version> get_versions(const std::string& since, const std::string& until, daq::config::Version::QueryType type, bool skip_irrelevant);

    virtual void subscribe(const std::set<std::string>& class_names,
                           const std::map< std::string , std::set<std::string> >& objs,
			   ConfigurationImpl::notify cb, ConfigurationImpl::pre_notify pre_cb);
    virtual void unsubscribe();


  private:

    rdbconfig::rdb_server * m_server;
    config::map<rdbconfig::ClassInfo> m_class_info;
    std::atomic<bool> m_prefeteched_data;

    std::set<std::string> m_xcalls;

    RDBInfoReceiver * m_receiver;
    ConfigurationImpl::notify m_fn;
    ConfigurationImpl::pre_notify m_pre_fn;

    // used by RdbConfigObject::contained_in()
    config::map<config::map<std::string>> m_files_of_all_objects;
    tbb::queuing_rw_mutex m_files_of_all_objects_mutex;

    void
    clean_files_of_all_objects()
    {
      tbb::queuing_rw_mutex::scoped_lock lock(m_files_of_all_objects_mutex);
      m_files_of_all_objects.clear();
    }

    void create(const std::string * at_file, const ConfigObject * at_obj, const std::string& class_name, const std::string& id, ConfigObject& object);

    RdbConfigObject * new_object(const rdb::RDBObject& obj) noexcept;
    void fill_cache(rdb::RDBClassValueList_var& values) noexcept;

    static void inform(RDBCallbackInfo *) noexcept;

    void store_schema(const rdb::RDBClassDescription& info, const rdb::RDBNameList& superclasses, const rdb::RDBNameList& subclasses, bool direct_only);


  public:

    virtual void print_profiling_info() noexcept;


  private:

    std::atomic<std::uint64_t> m_number_of_get_object_calls;
    std::atomic<std::uint64_t> m_number_of_get_all_objects_calls;
    std::atomic<std::uint64_t> m_number_of_get_objects_by_query_calls;
    std::atomic<std::uint64_t> m_number_of_xget_all_objects_calls;
    std::atomic<std::uint64_t> m_number_of_xget_objects_by_query_calls;
    std::atomic<std::uint64_t> m_number_of_get_objects_by_path_calls;
    std::atomic<std::uint64_t> m_number_of_get_meta_data_info_calls;

};

#endif // RDBCONFIG_RDBCONFIGURATION_H_
