#ifndef _rdbconfig_report_exceptions_
#define _rdbconfig_report_exceptions_

#include <ipc/exceptions.h>

#include <ers/ers.h>
#include <ers2idl/ers2idl.h>

namespace rdbconfig {
  std::string mk_no_loaded_db(const char * config_method);
  std::string mk_no_server_exception_text(const char * config_method, const char * rdb_method);
  std::string mk_rdb_server_exception_text(const char * config_method, const char * rdb_method, const char * reason);
  std::string mk_session_exception_text(const char * config_method, const char * rdb_method, int64_t session);
  std::string mk_wrong_server_exception_text(const char * config_method, const std::string& server_name);
  std::string mk_corba_exception_text(const char * config_method, const char * rdb_method, const CORBA::SystemException& ex);
}

#define RDB_SERVER_METHOD( CODE , WHERE , WHAT )                                                                              \
{                                                                                                                             \
  try {                                                                                                                       \
    if(!m_server || !m_server->partition()) {                                                                                 \
      throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_no_loaded_db( WHERE ).c_str() ) );                                \
    }                                                                                                                         \
    try {                                                                                                                     \
      m_server->lookup();                                                                                                     \
    }                                                                                                                         \
    catch(daq::rdb::LookupFailed & ex) {                                                                                      \
      throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_no_server_exception_text( WHERE , WHAT ).c_str() ) );             \
    }                                                                                                                         \
    CODE ;                                                                                                                    \
  }                                                                                                                           \
  catch ( const rdb::SessionNotFound & ex ) {                                                                                 \
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_session_exception_text( WHERE , WHAT , ex.session_id ).c_str() ) ); \
  }                                                                                                                           \
  catch ( const rdb::CannotProceed & ex ) {                                                                                   \
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_rdb_server_exception_text( WHERE , WHAT , "" ).c_str(), *daq::idl2ers_issue(ex.issue) ) );    \
  }                                                                                                                           \
  catch ( const rdb::NotFound & ex ) {                                                                                        \
    auto issue = daq::idl2ers_issue(ex.issue);                                                                                \
    auto it = issue->parameters().find("name");                                                                               \
    const char * name = (it != issue->parameters().end() ? it->second.c_str() : "");                                          \
    const char * issue_id = issue->get_class_name();                                                                          \
    const char * type = (                                                                                                     \
        !strcmp(issue_id, "rdb::ClassNotFound") ? "class" :                                                                   \
        !strcmp(issue_id, "rdb::ObjectNotFound") ? "object" :                                                                 \
        !strcmp(issue_id, "rdb::AttributeNotFound") ? "attribute" :                                                           \
        !strcmp(issue_id, "rdb::RelationshipNotFound") ? "relationship" :                                                     \
        "" );                                                                                                                 \
    throw ( daq::config::NotFound( ERS_HERE, type, name, *issue ));                                                           \
  }                                                                                                                           \
  catch ( const CORBA::SystemException & ex ) {                                                                               \
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_corba_exception_text( WHERE , WHAT , ex ).c_str() ) );              \
  }                                                                                                                           \
}

#endif
