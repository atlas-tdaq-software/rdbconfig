#include <limits>
#include <sstream>
#include <stdexcept>

#include "rdbconfig/ClassInfo.h"

namespace rdbconfig {

  ClassInfo::~ClassInfo()
  {
    if(m_inited) {
      m_svan_list.out();
      m_mvan_list.out();
      m_rn_list.out();
      m_inited = false;
    }
  }

  unsigned long
  ClassInfo::get_pos(const rdb::RDBNameList_var& w, const char * what, const std::string& name) const
  {
    rdb::RDBNameList_var& where = const_cast<rdb::RDBNameList_var&>(w);
    for(unsigned long idx = 0; idx < where->length(); ++idx) {
      if(!strcmp(name.c_str(), static_cast<const char *>(where[idx]))) {
        return idx;
      }
    }

    if (what)
      {
        std::ostringstream s;
        s << "cannot find " << what << " \'" << name << '\'';
        throw std::runtime_error(s.str().c_str());
      }

    return std::numeric_limits<unsigned long>::max();
  }

  void
  ClassInfo::init(rdb::RDBNameList& san, rdb::RDBNameList& man, rdb::RDBNameList& rn)
  {
    m_svan_list = new rdb::RDBNameList(san);
    m_mvan_list = new rdb::RDBNameList(man);
    m_rn_list   = new rdb::RDBNameList(rn);
    m_inited    = true;
  }

}
