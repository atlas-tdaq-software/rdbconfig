#include <unistd.h>
#include <typeinfo>
#include <stdexcept>
#include <sstream>

#include <ers/ers.h>

#include <config/ConfigObject.h>

#include "rdbconfig/RdbConfiguration.h"
#include "rdbconfig/RdbConfigObject.h"
#include "rdbconfig/rdb_server.h"

#include "report_exceptions.h"


void
RdbConfigObject::clear() noexcept
{
  m_sav_list.replace(0, 0, nullptr);
  m_mav_list.replace(0, 0, nullptr);
  m_rvlist.replace(0, 0, nullptr);
}

RdbConfigObject::~RdbConfigObject() noexcept
{
  clear();
}

const std::string
RdbConfigObject::contained_in() const
{
  RdbConfiguration * conf = static_cast<RdbConfiguration *>(m_impl);

  tbb::queuing_rw_mutex::scoped_lock lock(conf->m_files_of_all_objects_mutex, false);

  if (conf->m_files_of_all_objects.empty())
    {
      lock.upgrade_to_writer();

      // check again since upgrade_to_writer may release and re-acquiring the lock
      if (conf->m_files_of_all_objects.empty())
        {
          try
            {
              rdb::RDBFileObjectsList_var info;

              RDB_SERVER_METHOD(
                {
                  if (!m_server->is_writer())
                    m_server->read_cursor()->get_files_of_all_objects(info);
                  else
                    m_server->write_cursor()->get_files_of_all_objects(m_server->write_session(), info);
                },
              "RdbConfigObject::contained_in", "get_files_of_all_objects")

              for (unsigned int i = 0; i < info->length(); i++)
                for (unsigned int j = 0; j < info[i].objects.length(); j++)
                  conf->m_files_of_all_objects[static_cast<const char *>(info[i].objects[j].classid)][static_cast<const char *>(info[i].objects[j].name)] = static_cast<const char *>(info[i].filename);

              ERS_DEBUG( 1 , "read information about objects location in " << info->length() << " files");
            }
          catch (daq::config::NotFound & nf_ex)
            {
              throw daq::config::Generic(ERS_HERE, nf_ex.what());
            }

          lock.downgrade_to_reader();
        }
    }

  auto cl = conf->m_files_of_all_objects.find(class_name());

  if (cl != conf->m_files_of_all_objects.end())
    {
      auto obj = cl->second.find(m_id);

      if (obj != cl->second.end())
        {
          return obj->second;
        }
      else
        {
          throw daq::config::NotFound( ERS_HERE, "object", m_id.c_str() );
        }
    }
  else
    {
      throw daq::config::NotFound( ERS_HERE, "class", m_class_name->c_str() );
    }
}

unsigned long RdbConfigObject::m_number_of_cget_object_values_calls = 0;
unsigned long RdbConfigObject::m_number_of_xget_object_values_calls = 0;

  // read all values in single call
  // this is faster than reading each
  // attribute and relationship in individual call

void
RdbConfigObject::get_object_values()
{
  if(!m_sav_list.release()) {
    rdb::RDBAttributeCompactValueList_var tmp_sav_list;   // single-value attributes
    rdb::RDBAttributeCompactValuesList_var tmp_mav_list;  // multi-values attributes
    rdb::RDBRelationshipCompactValueList_var tmp_rv_list;

    if(m_class_info.get_inited()) {
      m_number_of_cget_object_values_calls++;

      ERS_DEBUG(2, "call implementation method cget_object_values(\"" << m_id << '@' << class_name() << "\")");

      try {
        RDB_SERVER_METHOD (
          {
            if(!m_server->is_writer()) {
              m_server->read_cursor()->cget_object_values(
                const_cast<char *>(m_class_name->c_str()),
                const_cast<char *>(m_id.c_str()),
                tmp_sav_list,
                tmp_mav_list,
                tmp_rv_list
              );
            }
            else {
              m_server->write_cursor()->cget_object_values(
                m_server->write_session(),
                const_cast<char *>(m_class_name->c_str()),
                const_cast<char *>(m_id.c_str()),
                tmp_sav_list,
                tmp_mav_list,
                tmp_rv_list
              );
            }
          },
         "RdbConfigObject::get_object_values",
          "cget_object_values"
        )
      }
      catch(daq::config::NotFound & nf_ex) {
        throw daq::config::Generic(ERS_HERE, nf_ex.what());
      }
    }
    else {
      m_number_of_xget_object_values_calls++;

      ERS_DEBUG(2, "call implementation method xget_object_values(\"" << m_id << '@' << class_name() << "\")");

      rdb::RDBNameList_var sv_an;
      rdb::RDBNameList_var mv_an;
      rdb::RDBNameList_var rn;

      try {
        RDB_SERVER_METHOD (
          {
            if(!m_server->is_writer()) {
              m_server->read_cursor()->xget_object_values(
                const_cast<char *>(m_class_name->c_str()),
                const_cast<char *>(m_id.c_str()),
                sv_an,
                mv_an,
                rn,
                tmp_sav_list,
                tmp_mav_list,
                tmp_rv_list
              );
            }
            else {
              m_server->write_cursor()->xget_object_values(
                m_server->write_session(),
                const_cast<char *>(m_class_name->c_str()),
                const_cast<char *>(m_id.c_str()),
                sv_an,
                mv_an,
                rn,
                tmp_sav_list,
                tmp_mav_list,
                tmp_rv_list
              );
            }
          },
          "RdbConfigObject::get_object_values",
          "xget_object_values"
        )
      }
      catch(daq::config::NotFound & nf_ex) {
        throw daq::config::Generic(ERS_HERE, nf_ex.what());
      }

      m_class_info.init(sv_an, mv_an, rn);
    }

    m_sav_list.replace(tmp_sav_list.in().maximum(), tmp_sav_list.in().length(), tmp_sav_list.in().NP_data(), true);
    m_mav_list.replace(tmp_mav_list.in().maximum(), tmp_mav_list.in().length(), tmp_mav_list.in().NP_data(), true);
    m_rvlist.replace(tmp_rv_list.in().maximum(), tmp_rv_list.in().length(), tmp_rv_list.in().NP_data(), true);

    tmp_sav_list.inout().NP_norelease();
    tmp_mav_list.inout().NP_norelease();
    tmp_rv_list.inout().NP_norelease();
  }
}

rdb::DataUnion&
RdbConfigObject::get_attribute_value(const std::string& name)
{
  get_object_values();

  try {
    return m_sav_list[m_class_info.get_sva_pos(name)];
  }
  catch(std::exception& ex) {
    std::ostringstream text;
    text << ex.what() << " for object \'" << this << '\'';
    throw ( daq::config::Generic( ERS_HERE, text.str().c_str() ) );
  }
}

rdb::DataListUnion&
RdbConfigObject::get_attribute_values(const std::string& name)
{
  get_object_values();

  try {
    return m_mav_list[m_class_info.get_mva_pos(name)];
  }
  catch(std::exception& ex) {
    std::ostringstream text;
    text << ex.what() << " for object \'" << this << '\'';
    throw ( daq::config::Generic( ERS_HERE, text.str().c_str() ) );
  }
}

  // the method returns 0, if it failed, e.g. requested relationship does not exist or it is multi-value
  // the method returns pointer to the rdb object;
  // to avoid additional parameter the pointer to null-rdb-object is equal to the address of the RdbConfigObject

const rdb::RDBObject *
RdbConfigObject::get_relation_object(const std::string& name)
{
  get_object_values();

  try {
    unsigned long idx = m_class_info.get_rel_pos(name);
    return (m_rvlist[idx].length() > 0 ? &m_rvlist[idx][0] : nullptr);
  }
  catch(std::exception& ex) {
    std::ostringstream text;
    text << ex.what() << " for object \'" << this << '\'';
    throw ( daq::config::Generic( ERS_HERE, text.str().c_str() ) );
  }
}

rdb::RDBObjectList&
RdbConfigObject::get_relation_objects(const std::string& name)
{
  get_object_values();

  try {
    unsigned long idx = m_class_info.get_rel_pos(name);
    return m_rvlist[idx];
  }
  catch(std::exception& ex) {
    std::ostringstream text;
    text << ex.what() << " for object \'" << this << '\'';
    throw ( daq::config::Generic( ERS_HERE, text.str().c_str() ) );
  }
}


static std::string
get_attr_error_text(const std::string& name, const RdbConfigObject * obj)
{
  std::ostringstream text;
  text << "failed to get value of attribute \'" << name << "\' for object \'" << obj << '\'';
  return text.str();
}


void RdbConfigObject::get(const std::string& name, bool& value)
{
  try {
    value = get_attribute_value(name).du_bool();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, uint8_t& value)
{
  try {
    value = get_attribute_value(name).du_u8_int();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, int8_t& value)
{
  try {
    value = get_attribute_value(name).du_s8_int();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, uint16_t& value)
{
  try {
    value = get_attribute_value(name).du_u16_int();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, int16_t& value)
{
  try {
    value = get_attribute_value(name).du_s16_int();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, uint32_t& value)
{
  try {
    value = get_attribute_value(name).du_u32_int();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, int32_t& value)
{
  try {
    value = get_attribute_value(name).du_s32_int();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, uint64_t& value)
{
  try {
    value = get_attribute_value(name).du_u64_int();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, int64_t& value)
{
  try {
    value = get_attribute_value(name).du_s64_int();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, float& value)
{
  try {
    value = get_attribute_value(name).du_float();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, double& value)
{
  try {
    value = get_attribute_value(name).du_double();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::string& value)
{
  try {
    value = get_attribute_value(name).du_string();
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, ConfigObject& value)
{
  if(const rdb::RDBObject * obj = get_relation_object(name)) {
    value = static_cast<RdbConfiguration *>(m_impl)->new_object(*obj);
  }
  else {
    value = 0;
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<bool>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_bools_seq & seq = get_attribute_values(name).du_bools();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<uint8_t>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_u8_ints_seq & seq = get_attribute_values(name).du_u8_ints();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<int8_t>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_s8_ints_seq & seq = get_attribute_values(name).du_s8_ints();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<uint16_t>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_u16_ints_seq & seq = get_attribute_values(name).du_u16_ints();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<int16_t>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_s16_ints_seq & seq = get_attribute_values(name).du_s16_ints();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<uint32_t>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_u32_ints_seq & seq = get_attribute_values(name).du_u32_ints();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<int32_t>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_s32_ints_seq & seq = get_attribute_values(name).du_s32_ints();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<uint64_t>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_u64_ints_seq & seq = get_attribute_values(name).du_u64_ints();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<int64_t>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_s64_ints_seq & seq = get_attribute_values(name).du_s64_ints();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<float>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_floats_seq & seq = get_attribute_values(name).du_floats();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<double>& value)
{
  value.clear();
  try {
    const rdb::DataListUnion::_du_doubles_seq & seq = get_attribute_values(name).du_doubles();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back(seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<std::string>& value)
{
  value.clear();
   try{
    const rdb::DataListUnion::_du_strings_seq & seq = get_attribute_values(name).du_strings();
    if(size_t len = seq.length()) { value.reserve(len); for(size_t i = 0; i < len; ++i) { value.push_back((const char *)seq[i]); } }
  }
  catch(daq::config::Generic& ex) {
    throw ( daq::config::Generic( ERS_HERE, get_attr_error_text(name, this).c_str(), ex ) );
  }
}

void RdbConfigObject::get(const std::string& name, std::vector<ConfigObject>& value)
{
  value.clear();
  const rdb::RDBObjectList& olist = get_relation_objects(name);
  for(unsigned int i = 0; i < olist.length(); i++) {
    value.push_back(ConfigObject(static_cast<RdbConfiguration *>(m_impl)->new_object(olist[i])));
  }
}

bool
RdbConfigObject::rel(const std::string& name, std::vector<ConfigObject>& value)
{
  get_object_values();

  unsigned long idx = m_class_info.get_pos(name);

  if (idx != std::numeric_limits<unsigned long>::max())
    {
      for (unsigned int i = 0; i < m_rvlist[idx].length(); i++)
        value.push_back(ConfigObject(static_cast<RdbConfiguration *>(m_impl)->new_object(m_rvlist[idx][i])));

      return true;
    }

  return false;
}


void
RdbConfigObject::referenced_by(std::vector<ConfigObject>& value, const std::string& rname, bool check_composite_only, unsigned long /*rlevel*/, const std::vector<std::string> * /*rclasses*/) const
{
  value.clear();

  rdb::RDBObject o;
  o.name = m_id.c_str();
  o.classid = m_class_name->c_str();

  rdb::RDBObjectList_var objs;

  ERS_DEBUG(2, "call implementation method get_referenced_by(\"" << m_id << '@' << class_name() << "\", \"" << rname << "\", " << std::boolalpha << check_composite_only << ')');

  RDB_SERVER_METHOD (
    {
      if(!m_server->is_writer()) {
        m_server->read_cursor()->get_referenced_by(o, rname.c_str(), check_composite_only, objs);
      }
      else {
        m_server->write_cursor()->get_referenced_by(m_server->write_session(), o, rname.c_str(), check_composite_only, objs);
      }
    },
    "RdbConfigObject::referenced_by",
    "get_referenced_by"
  )

  value.reserve(objs->length());

  for(unsigned int i = 0; i < objs->length(); i++) {
    value.push_back(ConfigObject(static_cast<RdbConfiguration *>(m_impl)->new_object(objs[i])));
  }
}

void
RdbConfigObject::set_attribute_value(const std::string& name, const char * type, rdb::DataUnion * value, rdb::DataListUnion * values)
{
  if(!m_server->is_writer()) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfigObject::set_attribute_value", m_server->name()).c_str() ) );
  }

  try {
    rdb::RDBObject o;
    o.name = m_id.c_str();
    o.classid = m_class_name->c_str();

    if(value) {
      RDB_SERVER_METHOD (
        {
          m_server->write_cursor()->set_attribute_value(m_server->write_session(), o, name.c_str(), *value);
        },
        "RdbConfigObject::set_attribute_value",
        "set_attribute_value"
      )

      get_attribute_value(name) = *value;
    }
    else {
      RDB_SERVER_METHOD (
        {
          m_server->write_cursor()->set_attribute_values(m_server->write_session(), o, name.c_str(), *values);
        },
        "RdbConfigObject::set_attribute_values",
        "set_attribute_values"
      )

      get_attribute_values(name) = *values;
    }
  }
  catch(daq::config::Generic& ex) {
    std::ostringstream text;
    text << "failed to set value (c++ type -> \'" << type << "\') of attribute \'" << name << " for object \'" << this << '\'';
    throw ( daq::config::Generic( ERS_HERE, text.str().c_str(), ex ) );
  }
}


void RdbConfigObject::set(const std::string& name, bool value)
{
  rdb::DataUnion d;
  d.du_bool(value);
  set_attribute_value(name, "bool", &d, 0);
}

void RdbConfigObject::set(const std::string& name, uint8_t value)
{
  rdb::DataUnion d;
  d.du_u8_int(value);
  set_attribute_value(name, "uint8_t", &d, 0);
}

void RdbConfigObject::set(const std::string& name, int8_t value)
{
  rdb::DataUnion d;
  d.du_s8_int(value);
  set_attribute_value(name, "int8_t", &d, 0);
}

void RdbConfigObject::set(const std::string& name, uint16_t value)
{
  rdb::DataUnion d;
  d.du_u16_int(value);
  set_attribute_value(name, "uint16_t", &d, 0);
}

void RdbConfigObject::set(const std::string& name, int16_t value)
{
  rdb::DataUnion d;
  d.du_s16_int(value);
  set_attribute_value(name, "int16_t", &d, 0);
}

void RdbConfigObject::set(const std::string& name, uint32_t value)
{
  rdb::DataUnion d;
  d.du_u32_int(value);
  set_attribute_value(name, "uint32_t", &d, 0);
}

void RdbConfigObject::set(const std::string& name, int32_t value)
{
  rdb::DataUnion d;
  d.du_s32_int(value);
  set_attribute_value(name, "int32_t", &d, 0);
}

void RdbConfigObject::set(const std::string& name, uint64_t value)
{
  rdb::DataUnion d;
  d.du_u64_int(value);
  set_attribute_value(name, "uint64_t", &d, 0);
}

void RdbConfigObject::set(const std::string& name, int64_t value)
{
  rdb::DataUnion d;
  d.du_s64_int(value);
  set_attribute_value(name, "int64_t", &d, 0);
}

void RdbConfigObject::set(const std::string& name, float value)
{
  rdb::DataUnion d;
  d.du_float(value);
  set_attribute_value(name, "float", &d, 0);
}

void RdbConfigObject::set(const std::string& name, double value)
{
  rdb::DataUnion d;
  d.du_double(value);
  set_attribute_value(name, "double", &d, 0);
}

void RdbConfigObject::set(const std::string& name, const std::string& value)
{
  rdb::DataUnion d;
  d.du_string(value.c_str());
  set_attribute_value(name, "const std::string&", &d, 0);
}

void RdbConfigObject::set_enum(const std::string& name, const std::string& value)
{
  set(name, value);
}

void RdbConfigObject::set_date(const std::string& name, const std::string& value)
{
  set(name, value);
}

void RdbConfigObject::set_time(const std::string& name, const std::string& value)
{
  set(name, value);
}

void RdbConfigObject::set_class(const std::string& name, const std::string& value)
{
  set(name, value);
}

void RdbConfigObject::set(const std::string& name, const std::vector<bool>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_bools_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<bool>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_bools(seq);

  set_attribute_value(name, "const std::vector<bool>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<uint8_t>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_u8_ints_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<uint8_t>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_u8_ints(seq);

  set_attribute_value(name, "const std::vector<uint8_t>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<int8_t>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_s8_ints_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<int8_t>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_s8_ints(seq);

  set_attribute_value(name, "const std::vector<int8_t>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<uint16_t>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_u16_ints_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<uint16_t>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_u16_ints(seq);

  set_attribute_value(name, "const std::vector<uint16_t>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<int16_t>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_s16_ints_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<int16_t>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_s16_ints(seq);

  set_attribute_value(name, "const std::vector<int16_t>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<uint32_t>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_u32_ints_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<uint32_t>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_u32_ints(seq);

  set_attribute_value(name, "const std::vector<uint32_t>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<int32_t>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_s32_ints_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<int32_t>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_s32_ints(seq);

  set_attribute_value(name, "const std::vector<int32_t>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<uint64_t>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_u64_ints_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<uint64_t>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_u64_ints(seq);

  set_attribute_value(name, "const std::vector<uint64_t>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<int64_t>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_s64_ints_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<int64_t>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_s64_ints(seq);

  set_attribute_value(name, "const std::vector<int64_t>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<float>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_floats_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<float>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_floats(seq);

  set_attribute_value(name, "const std::vector<float>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<double>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_doubles_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<double>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = *i;
  }
  d.du_doubles(seq);

  set_attribute_value(name, "const std::vector<double>&", 0, &d);
}

void RdbConfigObject::set(const std::string& name, const std::vector<std::string>& value)
{
  rdb::DataListUnion d;
  rdb::DataListUnion::_du_strings_seq seq;
  seq.length(value.size());
  unsigned int count = 0;
  for(std::vector<std::string>::const_iterator i = value.begin(); i != value.end(); ++i) {
    seq[count++] = (*i).c_str();
  }
  d.du_strings(seq);

  set_attribute_value(name, "const std::vector<std::string>&", 0, &d);
}

void RdbConfigObject::set_enum(const std::string& name, const std::vector<std::string>& value)
{
  RdbConfigObject::set(name, value);
}

void RdbConfigObject::set_date(const std::string& name, const std::vector<std::string>& value)
{
  RdbConfigObject::set(name, value);
}

void RdbConfigObject::set_time(const std::string& name, const std::vector<std::string>& value)
{
  RdbConfigObject::set(name, value);
}

void RdbConfigObject::set_class(const std::string& name, const std::vector<std::string>& value)
{
  RdbConfigObject::set(name, value);
}

void
RdbConfigObject::set_relationship_value(const std::string& name, const ConfigObject * value, const std::vector<const ConfigObject*> * values, bool skip_non_null_check)
{
  if(!m_server->is_writer()) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfigObject::set_relationship_value", m_server->name()).c_str() ) );
  }

  try {
    rdb::RDBObject o;
    o.name = m_id.c_str();
    o.classid = m_class_name->c_str();

    rdb::RDBObjectList d;

    if(value) {
      d.length(1);
      d[0].name = value->UID().c_str();
      d[0].classid = value->class_name().c_str();
    }

    if(values && !values->empty()) {
      d.length(values->size());
      for(unsigned int i = 0; i < values->size(); ++i) {
        d[i].name = (*values)[i]->UID().c_str();
        d[i].classid = (*values)[i]->class_name().c_str();
      }
    }

    RDB_SERVER_METHOD (
      {
        m_server->write_cursor()->set_objects_of_relationship(m_server->write_session(), o, name.c_str(), d, skip_non_null_check);
      },
      "RdbConfigObject::set",
      "set_objects_of_relationship"
    )

    get_relation_objects(name) = d;
  }
  catch(daq::config::Generic& ex) {
    std::ostringstream text;
    text << "failed to set ";
    if(value) { text << "object \'" << value << '\''; }
    else      { text << values->size() << " objects";  }
    text << " as relationship value for object \'" << this << '\'';
    throw ( daq::config::Generic( ERS_HERE, text.str().c_str(), ex ) );
  }
}

void RdbConfigObject::set(const std::string& name, const ConfigObject * value, bool skip_non_null_check)
{
  set_relationship_value(name, value, 0, skip_non_null_check);
}

void RdbConfigObject::set(const std::string& name, const std::vector<const ConfigObject*>& value, bool skip_non_null_check)
{
  set_relationship_value(name, 0, &value, skip_non_null_check);
}

void
RdbConfigObject::move(const std::string& at)
{
  try
    {
      rdb::RDBObject o;
      o.name = m_id.c_str();
      o.classid = m_class_name->c_str();

      RDB_SERVER_METHOD (
        {
          m_server->write_cursor()->move_object(m_server->write_session(), o, at.c_str());
        },
        "RdbConfigObject::move",
        "move_object"
      )
    }
  catch (daq::config::Generic& ex)
    {
      std::ostringstream text;
      text << "failed to move object \'" << this << " to file \'" << at << '\'';
      throw(daq::config::Generic(ERS_HERE, text.str().c_str(), ex ) );
    }

  static_cast<RdbConfiguration *>(m_impl)->clean_files_of_all_objects();
}

void
RdbConfigObject::rename(const std::string& new_id)
{
  try
    {
      rdb::RDBObject o;
      o.name = m_id.c_str();
      o.classid = m_class_name->c_str();

      RDB_SERVER_METHOD (
        {
          m_server->write_cursor()->rename_object(m_server->write_session(), o, new_id.c_str());
        },
        "RdbConfigObject::rename",
        "rename_object"
      )
    }
  catch (daq::config::Generic& ex)
    {
      std::ostringstream text;
      text << "failed to rename object \'" << this << " to \'" << new_id << '\'';
      throw(daq::config::Generic(ERS_HERE, text.str().c_str(), ex ) );
    }

  static_cast<RdbConfiguration *>(m_impl)->clean_files_of_all_objects();
}

void RdbConfigObject::reset()
{
  rdb::RDBObject_var obj;
  rdb::RDBClassValueList_var values;
  rdb::RDBNameList cnames;
  cnames.length(0);

  static_cast<RdbConfiguration *>(m_impl)->m_number_of_get_object_calls++;

  ERS_DEBUG(2, "call implementation method xget_object(\"" << m_id << '@' << class_name() << "\")");

  try
  {
    RDB_SERVER_METHOD (
      {
        if(!m_server->is_writer()) {
          m_server->read_cursor()->xget_object(m_class_name->c_str(), true, m_id.c_str(), obj, 0, cnames, values);
        }
        else {
          m_server->write_cursor()->xget_object(m_server->write_session(), m_class_name->c_str(), true, m_id.c_str(), obj, 0, cnames, values);
        }
      },
      "RdbConfiguration::get",
      "xget_object"
    )

    static_cast<RdbConfiguration *>(m_impl)->fill_cache(values);
    m_state = daq::config::Valid;
  }
  catch(const daq::config::NotFound&)
  {
    m_state = daq::config::Deleted;
  }
  catch(const daq::config::Generic& ex)
  {
    ers::error( daq::config::Generic(ERS_HERE, "failed to reset object after abort() operation", ex));
    m_state = daq::config::Deleted;
  }
}


std::ostream&
operator<<(std::ostream& s, const RdbConfigObject * obj)
{
  if(obj) {
    s << obj->m_id << '@' << obj->class_name();
  }
  else {
    s << "(null)";
  }
  return s;
}

