#include <iostream>
#include <mutex>
#include <ipc/partition.h>

#include <config/Errors.h>

#include <rdb/errors.h>
#include <rdb/rdb_info.h>

#include "rdbconfig/rdb_server.h"

#include "report_exceptions.h"

namespace rdbconfig {

  rdb_server::rdb_server(const std::string& partition_name, const std::string& server_name) :
    m_partition       (partition_name.empty() ? new IPCPartition() : new IPCPartition(partition_name.c_str())),
    m_server_name     (server_name),
    m_inited          (false),
    m_session         (0),
    m_ping_cb         (nullptr),
    m_interval_tp     (std::chrono::steady_clock::now()),
    m_number_of_calls (0)
  {
    lookup();
  }

  rdb_server::~rdb_server()
  {
    clean();
  }

  bool
  rdb_server::is_valid() const
  {
    return (m_partition != 0 && (m_is_writer ? !CORBA::is_nil(m_write_cursor) : !CORBA::is_nil(m_read_cursor)));
  }

  void
  rdb_server::clean()
  {
    if(m_inited && m_is_writer) {
      try {
        rdb::ProcessInfoHelper::refresh(m_proc_info);
        m_write_cursor->close_session( m_session, m_proc_info );
      }
      catch ( rdb::CannotProceed & ex ) {
        ers::error( daq::config::Generic( ERS_HERE, rdbconfig::mk_rdb_server_exception_text( "rdb_server::clean()" , "close_session" , "" ).c_str(), *daq::idl2ers_issue(ex.issue) ) );
      }
      catch ( const CORBA::SystemException & ex ) {
        ers::error( daq::config::Generic( ERS_HERE, rdbconfig::mk_corba_exception_text( "rdb_server::clean()" , "close_session" , ex ).c_str() ) );
      }
      catch ( const ers::Issue& ex) {
        ers::error( daq::config::Generic( ERS_HERE, rdbconfig::mk_rdb_server_exception_text( "rdb_server::clean()" , "close_session" , "" ).c_str(), ex ) );
      }

      m_ping_cb->_destroy();
      m_ping_cb = nullptr;
    }

    delete m_partition;
    m_partition = nullptr;

    m_inited = false;
  }

  void
  rdb_server::lookup()
  {
    // dedicated RDB client timeout (different from IPC generic timeout)
    static CORBA::ULong timeout(0);

    // the client is efficient if it makes smaller amount of calls in given time interval (in ms)
    static int64_t efficiency_max_calls(5000);
    static int64_t efficiency_interval(10000);

    // set RDB client timeout and efficiency
    static std::once_flag flag;
    std::call_once(flag, []()
      {
        if (const char * val = getenv("TDAQ_RDB_CLIENT_TIMEOUT"))
          timeout = strtoul(val, nullptr, 0);

        if (const char * val = getenv("TDAQ_RDB_CLIENT_EFFICIENCY_INTERVAL"))
          efficiency_interval = strtol(val, nullptr, 0);

        if (const char * val = getenv("TDAQ_RDB_CLIENT_EFFICIENCY_MAX_CALLS"))
          efficiency_max_calls = strtol(val, nullptr, 0);
      }
    );

    if (!m_inited)
      {
        try
          {
            m_read_cursor = m_partition->lookup<rdb::cursor>(m_server_name.c_str());
            m_is_writer = false;
          }
        catch (ers::Issue&)
          {
            try
              {
                m_write_cursor = m_partition->lookup<rdb::writer>(m_server_name.c_str());
                m_is_writer = true;
                rdb::ProcessInfoHelper::set(m_proc_info);
                m_ping_cb = new RDBWriterPingCB();
                m_session = m_write_cursor->open_session(m_proc_info, m_ping_cb->_this());
                m_ping_cb->set_session_id(m_session);
              }
            catch (CORBA::SystemException&)
              {
                throw;  // to be caught by caller
              }
            catch (ers::Issue &ex)
              {
                throw daq::rdb::LookupFailed(ERS_HERE, m_server_name.c_str(), m_partition->name().c_str(), ex);
              }
          }

        if (timeout)
          {
            if (m_is_writer == false)
              omniORB::setClientCallTimeout(m_read_cursor, timeout);
            else
              omniORB::setClientCallTimeout(m_write_cursor, timeout);

            ERS_DEBUG(1, "set rdb client timeout " << timeout << " ms");
          }

        m_inited = true;
      }
    else
      {
        if (m_is_writer == false)
          {
            try
              {
                m_read_cursor = m_partition->lookup<rdb::cursor>(m_server_name.c_str());
              }
            catch (ers::Issue &ex)
              {
                throw daq::rdb::LookupFailed(ERS_HERE, m_server_name.c_str(), m_partition->name().c_str(), ex);
              }
          }
        else
          {
            try
              {
                m_write_cursor = m_partition->lookup<rdb::writer>(m_server_name.c_str());
              }
            catch (ers::Issue &ex)
              {
                throw daq::rdb::LookupFailed(ERS_HERE, m_server_name.c_str(), m_partition->name().c_str(), ex);
              }
          }
      }

    if (efficiency_max_calls)
      {
        if (++m_number_of_calls >= efficiency_max_calls)
          {
            std::lock_guard<std::mutex> scoped_lock(m_efficiency_mutex);

            auto tp = std::chrono::steady_clock::now();
            auto interval = std::chrono::duration_cast<std::chrono::milliseconds>(tp - m_interval_tp).count();

            if (interval < efficiency_interval)
              {
                std::ostringstream text;
                text << "inefficient RDB access suspected: " << m_number_of_calls << " calls on server \"" << m_server_name << "\" within " << interval << " ms (check RDB prefetch configuration and used DAL algorithms)";
                ers::warning(daq::config::Generic(ERS_HERE, text.str().c_str()));
                m_number_of_calls = std::numeric_limits<int64_t>::min(); // report once per application
              }
            else
              {
                m_interval_tp = tp;
                m_number_of_calls = 1;
              }
          }
      }
  }

}
