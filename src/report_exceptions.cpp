#include <iostream>

#include <config/Errors.h>

#include "report_exceptions.h"


namespace rdbconfig {

  static std::string
  mk_header(const char * config_method, const char * rdb_method)
  {
    return ( std::string("method \'") + config_method + "()\' failed calling \'RDBServer::" + rdb_method + "()\'\n\twas caused by: " );
  }

  std::string
  mk_no_loaded_db(const char * config_method)
  {
    return ( std::string("no loaded database (call \'") + config_method + "\')" );
  }

  std::string
  mk_no_server_exception_text(const char * config_method, const char * rdb_method)
  {
    return ( mk_header(config_method, rdb_method) + "lookup() on RDB server failed" );
  }

  std::string
  mk_rdb_server_exception_text(const char * config_method, const char * rdb_method, const char * reason)
  {
    return ( mk_header(config_method, rdb_method) + reason );
  }

  std::string mk_session_exception_text(const char * config_method, const char * rdb_method, int64_t session)
  {
    std::ostringstream s;
    s << "session " << session << " is not found";
    return ( mk_header(config_method, rdb_method) + s.str() );
  }

  std::string mk_wrong_server_exception_text(const char * config_method, const std::string& server_name)
  {
    return ( std::string("method \'") + config_method + "()\' cannot be called on read-only server \'" + server_name + '\'' );
  }

  std::string mk_corba_exception_text(const char * config_method, const char * rdb_method, const CORBA::SystemException& ex)
  {
    std::ostringstream text;
    text << mk_header(config_method, rdb_method) << "CORBA exception \'" << &ex << '\'';
    return text.str();
  }

}
