#include <unistd.h>
#include <iostream>

#include <thread>
#include <memory>

#include <ers/ers.h>

#include <daq_tokens/acquire.h>
#include <daq_tokens/common.h>

#include <ipc/core.h>

#include "config/Change.h"
#include "config/Configuration.h"
#include "config/ConfigObject.h"
#include <config/Schema.h>

#include "rdbconfig/RdbConfiguration.h"
#include "rdbconfig/RdbConfigObject.h"
#include "rdbconfig/rdb_server.h"

#include "report_exceptions.h"


  // to be used as plug-in

extern "C" ConfigurationImpl * _rdbconfig_creator_ (const std::string& spec, Configuration * db) {
  try {
    std::unique_ptr<RdbConfiguration> impl(new RdbConfiguration(db));
    if(!spec.empty()) { impl->open_db(spec); }
    return impl.release();
  }
  catch(daq::config::Exception & ex) {
    throw daq::config::Generic(ERS_HERE, "rdbconfig initialization error", ex);
  }
  catch(...) {
    throw daq::config::Generic(ERS_HERE, "rdbconfig initialization error:\n***** caught unknown exception *****");
  }
}

RdbConfiguration::RdbConfiguration(Configuration * db) noexcept :
  ConfigurationImpl(db),
  m_server(nullptr),
  m_prefeteched_data(false),
  m_receiver(nullptr),
  m_fn(nullptr),
  m_pre_fn(nullptr),
  m_number_of_get_object_calls(0),
  m_number_of_get_all_objects_calls(0),
  m_number_of_get_objects_by_query_calls(0),
  m_number_of_xget_all_objects_calls(0),
  m_number_of_xget_objects_by_query_calls(0),
  m_number_of_get_objects_by_path_calls(0),
  m_number_of_get_meta_data_info_calls(0)
{
  if(IPCCore::isInitialised() == false) {
    try {
      ERS_DEBUG(0, "IPC was not initialized, force IPCCore::init()");
      int argc(0);
      IPCCore::init(argc, 0);
    }
    catch(ers::Issue & ex) {
      ers::warning(ers::Message(ERS_HERE, ex));
    }
  }
}


RdbConfiguration::~RdbConfiguration()
{
  try {
    unsubscribe();
  }
  catch(daq::config::Generic& ex) {
    ers::error( ex );
  }

  close_db();
  delete m_server;
}


RdbConfigObject *
RdbConfiguration::new_object(const rdb::RDBObject& obj) noexcept
{
  RdbConfigObjectNames o(obj);
  return insert_object<RdbConfigObject>(o, o.m_object_id, o.m_class_name);
}


void
RdbConfiguration::fill_cache(rdb::RDBClassValueList_var& values) noexcept
{
  ERS_DEBUG(3, "Got objects of " << values->length() << " classes to be put to RDB config cache");

  std::string class_name;
  RdbConfigObjectInfo obj_info;

  for(unsigned int j = 0; j < values->length(); ++j) {
    auto& v(values[j]);

    class_name.assign(static_cast<const char *>(v.name));

    ERS_DEBUG(3, " * got " << v.objects.length() << " objects of class \'" << class_name << "\' to be put to RDB config cache");

    obj_info.m_class_info = &m_class_info[class_name];

    if(obj_info.m_class_info->get_inited() == false) {
      obj_info.m_class_info->init(v.sv_attributes, v.mv_attributes, v.relationships);
    }

    for(unsigned int i = 0; i < v.objects.length(); ++i) {
      obj_info.m_object_id.assign(static_cast<const char *>(v.objects[i].name));

      RdbConfigObject * o = insert_object<RdbConfigObject>(obj_info, obj_info.m_object_id, class_name);

      if (!o->m_sav_list.release())
        {
          ERS_DEBUG(3, "  - set values for " << obj_info.m_object_id << '@' << class_name << " object");

          o->m_sav_list.replace(v.objects[i].av.maximum(), v.objects[i].av.length(), v.objects[i].av.NP_data(), true);
          o->m_mav_list.replace(v.objects[i].avs.maximum(), v.objects[i].avs.length(), v.objects[i].avs.NP_data(), true);
          o->m_rvlist.replace(v.objects[i].rv.maximum(), v.objects[i].rv.length(), v.objects[i].rv.NP_data(), true);

          v.objects[i].av.NP_norelease();
          v.objects[i].avs.NP_norelease();
          v.objects[i].rv.NP_norelease();
        }
    }
  }
}


void
RdbConfiguration::open_db(const std::string& db_name)
{
  std::string partition_name;
  std::string server_name;
  std::string::size_type idx = db_name.find("::");
  std::string::size_type idx2 = db_name.find('@');

    // partitionX::RDBname
  if(idx != std::string::npos) {
    partition_name = db_name.substr(0, idx);
    server_name = db_name.substr(idx + 2);
  }
    // RDBname@partitionX
  else if(idx2 != std::string::npos) {
    server_name = db_name.substr(0, idx2);
    partition_name = db_name.substr(idx2 + 1);
  }
  else {
    const char * tdaq_partition = getenv("TDAQ_PARTITION");

    if(tdaq_partition && *tdaq_partition) {
      partition_name = tdaq_partition;
    }

    server_name = db_name;
  }

  if(server_name.empty()) {
    std::ostringstream text;
    text << "cannot open db \'" << db_name << "\' -> server is not defined";
    close_db();
    throw ( daq::config::Generic( ERS_HERE, text.str().c_str() ) );
  }

    // find server

  try {
    if(m_server) { delete m_server; m_server = nullptr; }
    m_server = new rdbconfig::rdb_server(partition_name, server_name);
  }
  catch ( const CORBA::SystemException & ex ) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_corba_exception_text( "RdbConfiguration::open_db" , "open_session" , ex ).c_str() ) );
  }
  catch(daq::rdb::LookupFailed & ex) {
    std::ostringstream text;
    text << "cannot open db \'" << db_name << "\' -> lookup() failed";
    throw ( daq::config::Generic( ERS_HERE, text.str().c_str(), ex ) );
  }

  if(m_server->is_valid() == false) {
    std::ostringstream text;
    text << "cannot open db \'" << db_name << "\' -> cannot find database server \'" << server_name << '\'';
    delete m_server;
    m_server = nullptr;
    throw daq::config::Generic( ERS_HERE, text.str().c_str() );
  }

  clean_files_of_all_objects();
  m_xcalls.clear();
}

void
RdbConfiguration::create(const std::string& db_name, const std::list<std::string>& includes)
{
  if (!m_server->is_writer())
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfiguration::create", m_server->name()).c_str() ) );

  // create new database file
  RDB_SERVER_METHOD ( { m_server->write_cursor()->create_database(m_server->write_session(), db_name.c_str()); }, "RdbConfiguration::create", "create_database" )

  // add includes
  for (const auto& i : includes)
    RDB_SERVER_METHOD ( { m_server->write_cursor()->database_add_file(m_server->write_session(), db_name.c_str(), i.c_str()); }, "RdbConfiguration::create", "database_add_file" )

  clean_files_of_all_objects();
}

bool
RdbConfiguration::is_writable(const std::string& db_name)
{
  if(!m_server->is_writer()) {
    return false;
  }

  RDB_SERVER_METHOD (
    {
      return m_server->write_cursor()->is_database_writable(m_server->write_session(), db_name.c_str());
    },
    "RdbConfiguration::is_writable",
    "is_database_writable"
  )
}

void
RdbConfiguration::add_include(const std::string& db_name, const std::string& include)
{
  if(!m_server->is_writer()) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfiguration::add_include", m_server->name()).c_str() ) );
  }

  RDB_SERVER_METHOD (
    {
      m_server->write_cursor()->database_add_file(m_server->write_session(), db_name.c_str(), include.c_str());
    },
    "RdbConfiguration::add_include",
    "database_add_file"
  )

  clean_files_of_all_objects();
}


void
RdbConfiguration::remove_include(const std::string& db_name, const std::string& include)
{
  if(!m_server->is_writer()) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfiguration::remove_include", m_server->name()).c_str() ) );
  }

  rdb::RDBClassChangesList_var rdb_changes; // removed objects , if some files will be closed

  RDB_SERVER_METHOD (
    {
      m_server->write_cursor()->database_remove_file(m_server->write_session(), db_name.c_str(), include.c_str(), rdb_changes);
    },
    "RdbConfiguration::remove_include",
    "database_remove_file"
  )

  if(rdb_changes->length()) {
    std::vector<ConfigurationChange *> changes;
    
    for(unsigned int idx = 0; idx < rdb_changes->length(); idx++) {
      const char * name = static_cast<const char *>(rdb_changes[idx].name);
      for(unsigned int i3 = 0; i3 < rdb_changes[idx].removed.length(); i3++) {
        const char * id = static_cast<const char *>(rdb_changes[idx].removed[i3]);
	ConfigurationChange::add(changes, name, id, '-');
      }
    }

    if(!changes.empty()) {
      m_conf->update_cache(changes);
      ConfigurationChange::clear(changes);
    }
  }

  clean_files_of_all_objects();
}


void
RdbConfiguration::get_includes(const std::string& db_name, std::list<std::string>& includes) const
{
  rdb::RDBNameList_var ilist;

  RDB_SERVER_METHOD (
    {
      if(!m_server->is_writer()) {
        m_server->read_cursor()->database_get_files(db_name.c_str(), ilist);
      }
      else {
        m_server->write_cursor()->database_get_files(m_server->write_session(), db_name.c_str(), ilist);
      }
    },
    "RdbConfiguration::get_includes",
    "get_includes"
  )

  for(unsigned int idx = 0; idx < ilist->length(); ++idx) {
    includes.push_back((const char *)ilist[idx]);
  }
}

void
RdbConfiguration::get_updated_dbs(std::list<std::string>& dbs) const
{
  if(!m_server->is_writer()) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfiguration::get_updated_dbs", m_server->name()).c_str() ) );
  }

  rdb::RDBNameList_var dbs_list;
  rdb::RDBNameList_var dummu1, dummu2;

  RDB_SERVER_METHOD (
    {
      m_server->write_cursor()->get_modified_databases(m_server->write_session(), dbs_list, dummu1, dummu2);
    },
    "RdbConfiguration::get_updated_dbs",
    "get_modified_databases"
  )

  for(unsigned int idx = 0; idx < dbs_list->length(); ++idx) {
    dbs.push_back((const char *)dbs_list[idx]);
  }
}


void
RdbConfiguration::set_commit_credentials(const std::string& user, const std::string& password)
{
  if(!m_server->is_writer()) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfiguration::set_commit_credentials", m_server->name()).c_str() ) );
  }

  RDB_SERVER_METHOD (
    {
      m_server->write_cursor()->set_commit_credentials(m_server->write_session(), user.c_str(), password.c_str());
    },
    "RdbConfiguration::set_commit_credentials",
    "set_commit_credentials"
  )
}


void
RdbConfiguration::commit(const std::string& why)
{
  if(!m_server->is_writer()) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfiguration::commit", m_server->name()).c_str() ) );
  }

  std::string token;

  if (daq::tokens::enabled())
    try
      {
        token = daq::tokens::acquire(daq::tokens::Mode::Reuse);
      }
    catch(const ers::Issue& ex)
      {
        std::ostringstream text;
        text << "cannot acquire daq token:\n\nwas caused by: " << ex;
        throw ( daq::config::Generic( ERS_HERE, text.str().c_str() ) );
      }

  RDB_SERVER_METHOD (
    {
      m_server->write_cursor()->commit_changes(m_server->write_session(), token.c_str(), why.c_str());
    },
    "RdbConfiguration::commit",
    "commit_changes"
  )
}

void
RdbConfiguration::abort()
{
  if(!m_server->is_writer()) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfiguration::commit", m_server->name()).c_str() ) );
  }

  RDB_SERVER_METHOD (
    {
      m_server->write_cursor()->abort_changes(m_server->write_session());
    },
    "RdbConfiguration::abort",
    "abort_changes"
  )

  clean_files_of_all_objects();
}


static void
add_attributes(daq::config::class_t * c, const rdb::RDBAttributeList& l, bool direct_only)
{
  for (unsigned int i = 0; i < l.length(); ++i)
    if (direct_only == false || l[i].isDirect)
      {
        std::string type(static_cast<const char*>(l[i].type));
        const_cast<std::vector<daq::config::attribute_t>&>(c->p_attributes).push_back(
          daq::config::attribute_t(
            static_cast<const char *>(l[i].name),
            (
              type == "string"  ? daq::config::string_type :
              type == "enum"    ? daq::config::enum_type   :
              type == "bool"    ? daq::config::bool_type   :
              type == "s8"      ? daq::config::s8_type     :
              type == "u8"      ? daq::config::u8_type     :
              type == "s16"     ? daq::config::s16_type    :
              type == "u16"     ? daq::config::u16_type    :
              type == "s32"     ? daq::config::s32_type    :
              type == "u32"     ? daq::config::u32_type    :
              type == "s64"     ? daq::config::s64_type    :
              type == "u64"     ? daq::config::u64_type    :
              type == "float"   ? daq::config::float_type  :
              type == "double"  ? daq::config::double_type :
              type == "date"    ? daq::config::date_type   :
              type == "time"    ? daq::config::time_type   :
              daq::config::class_type
            ),
            static_cast<const char *>(l[i].range),
            (
              l[i].intFormat == rdb::int_format_na  ? daq::config::na_int_format  :
              l[i].intFormat == rdb::int_format_dec ? daq::config::dec_int_format :
              l[i].intFormat == rdb::int_format_hex ? daq::config::hex_int_format :
              daq::config::oct_int_format
            ),
            l[i].isNotNull,
            l[i].isMultiValue,
            static_cast<const char *>(l[i].initValue),
            static_cast<const char *>(l[i].description)
          )
        );
      }
}

static void
add_relationships(daq::config::class_t * c, const rdb::RDBRelationshipList& l, bool direct_only)
{
  for (unsigned int i = 0; i < l.length(); ++i)
    if (direct_only == false || l[i].isDirect)
      {
        const_cast<std::vector<daq::config::relationship_t>&>(c->p_relationships).push_back(
          daq::config::relationship_t(
            static_cast<const char *>(l[i].name),
            static_cast<const char *>(l[i].classid),
            (l[i].isNotNull == false),
            l[i].isMultiValue,
            l[i].isAggregation,
            static_cast<const char *>(l[i].description)
          )
      );
    }
}

void
RdbConfiguration::prefetch_data()
{
  rdb::RDBClassValueList_var values;

  ERS_DEBUG(2, "call implementation method prefetch_data()");

  RDB_SERVER_METHOD (
    {
      if(!m_server->is_writer()) {
        m_server->read_cursor()->prefetch_data(values);
      }
      else {
        m_server->write_cursor()->prefetch_data(m_server->write_session(), values);
      }
    },
    "RdbConfiguration::prefetch_data",
    "prefetch_data"
  )

  fill_cache(values);
  m_prefeteched_data = true;
}


void
RdbConfiguration::store_schema(const rdb::RDBClassDescription& info, const rdb::RDBNameList& superclasses, const rdb::RDBNameList& subclasses, bool direct_only)
{
  std::string class_name(static_cast<const char *>(info.name));

  daq::config::class_t * c = new daq::config::class_t(class_name, static_cast<const char *>(info.description), info.isAbstract);

  for(unsigned int i = 0; i < superclasses.length(); ++i)
    const_cast< std::vector<std::string>& >(c->p_superclasses).push_back(static_cast<const char *>(superclasses[i]));

  for(unsigned int i = 0; i < subclasses.length(); ++i)
    const_cast< std::vector<std::string>& >(c->p_subclasses).push_back(static_cast<const char *>(subclasses[i]));

  add_attributes(c, info.attributes, direct_only);
  add_relationships(c, info.relationships, direct_only);

  store_class_info(class_name, direct_only, c);
}

void
RdbConfiguration::prefetch_schema()
{
  rdb::RDBClassDescriptionList_var values;

  ERS_DEBUG(2, "call implementation method prefetch_schema()");

  RDB_SERVER_METHOD (
    {
      if(!m_server->is_writer()) {
        m_server->read_cursor()->prefetch_schema(values);
      }
      else {
        m_server->write_cursor()->prefetch_schema(m_server->write_session(), values);
      }
    },
    "RdbConfiguration::prefetch_schema",
    "prefetch_schema"
  )

  for (unsigned int idx = 0; idx < values->length(); ++idx)
    {
      const rdb::RDBClassDescription& info(values[idx]);

      store_schema(info, info.direct_superclasses, info.direct_subclasses, true);
      store_schema(info, info.all_superclasses, info.all_subclasses, false);
    }
}


static std::vector<daq::config::Version>
rdb2config(rdb::RDBRepositoryVersionList& in)
{
  std::vector<daq::config::Version> out;

  const auto len = in.length();

  out.reserve(len);

  for (unsigned int x = 0; x < len; x++)
    {
      const auto num = in[x].files.length();
      std::vector<std::string> files;
      files.reserve(num);
      for (unsigned int i = 0; i < num; i++)
        files.push_back(static_cast<const char *>(in[x].files[i]));

      out.emplace_back(static_cast<const char *>(in[x].id), static_cast<const char *>(in[x].user), static_cast<long int>(in[x].timestamp), static_cast<const char *>(in[x].comment), files);
    }

  return out;
}

std::vector<daq::config::Version>
RdbConfiguration::get_changes()
{
  rdb::RDBRepositoryVersionList_var versions;

  ERS_DEBUG(2, "call implementation method get_changes()");

  RDB_SERVER_METHOD (
    {
      if (!m_server->is_writer())
        versions = m_server->read_cursor()->get_changes();
      else
        versions = m_server->write_cursor()->get_changes();
    },
    "RdbConfiguration::get_changes",
    "get_changes"
  )

  return rdb2config(versions.inout());
}

std::vector<daq::config::Version>
RdbConfiguration::get_versions(const std::string& since, const std::string& until, daq::config::Version::QueryType type, bool skip_irrelevant)
{
  rdb::RDBRepositoryVersionList_var versions;

  ERS_DEBUG(2, "call implementation method get_changes()");

  rdb::VersionsQueryType query_type = (type == daq::config::Version::query_by_date ? rdb::query_by_date : (type == daq::config::Version::query_by_id ? rdb::query_by_id : rdb::query_by_tag));

  RDB_SERVER_METHOD (
    {
      if (!m_server->is_writer())
        versions = m_server->read_cursor()->get_versions(skip_irrelevant, query_type, since.c_str(), until.c_str());
      else
        versions = m_server->write_cursor()->get_versions(skip_irrelevant, query_type, since.c_str(), until.c_str());
    },
    "RdbConfiguration::get_versions",
    "get_versions"
  )

  return rdb2config(versions.inout());
}

void
RdbConfiguration::close_db()
{
  if(m_server && m_server->is_valid()) {
    m_server->clean();
  }

  clean_files_of_all_objects();

  m_prefeteched_data = false;
  m_xcalls.clear();
}

static void
vector2name_list(const std::vector<std::string> * v, rdb::RDBNameList& l)
{
  if(v) {
    unsigned int idx = 0;
    l.length(v->size());
    for(std::vector<std::string>::const_iterator i = v->begin(); i != v->end(); ++i) {
      l[idx++] = CORBA::string_dup((*i).c_str());
    }
  }
  else {
    l.length(0);
  }
}

#ifndef ERS_NO_DEBUG

static std::string
param2str(unsigned long rlevel, const std::vector<std::string> * rclasses)
{
  std::ostringstream s;
  s << ", ref-level = " << rlevel << ", rel-classes = \'";
  if(rclasses) {
    for( std::vector<std::string>::const_iterator i = rclasses->begin(); i != rclasses->end(); ++i) {
      if(i != rclasses->begin()) s << ", ";
      s << *i;
    }
  }
  else {
    s << "(null)";
  }
  s << '\'';

  return s.str();
}

#endif


static void
add_refs(std::string& out, unsigned long rlevel, const std::vector<std::string> * rclasses)
{
  out.push_back('~');
  out.append(std::to_string(rlevel));

  if (rclasses)
    {
      std::set<std::string> names(rclasses->begin(), rclasses->end());
      for (const auto& x : names)
        {
          out.push_back(':');
          out.append(x);
        }
    }
}

static std::string
obj_ref_to_str(const std::string& class_name, const std::string& id, unsigned long rlevel, const std::vector<std::string> * rclasses)
{
  std::string out = id;
  out.push_back('@');
  out.append(class_name);

  add_refs(out, rlevel, rclasses);

  return out;
}

static std::string
query_ref_to_str(const std::string &class_name, const std::string &query, unsigned long rlevel, const std::vector<std::string> *rclasses)
{
  std::string out = class_name;

  if (!query.empty())
    {
      out.push_back('+');
      out.append(query);
    }

  return out;
}


bool RdbConfiguration::test_object(const std::string& class_name, const std::string& oid, unsigned long rlevel, const std::vector<std::string> * rclasses)
{
  // try to find object in cache
  if(get_impl_object(class_name, oid)) return true;

  // read object from rdb server
  rdb::RDBObject_var obj;
  rdb::RDBClassValueList_var values;
  rdb::RDBNameList cnames; vector2name_list(rclasses, cnames);

  m_number_of_get_object_calls++;

  ERS_DEBUG(2, "call implementation method xget_object(" << oid << '@' << class_name << param2str(rlevel, rclasses) << ')');

  RDB_SERVER_METHOD (
    {
      if(!m_server->is_writer()) {
        m_server->read_cursor()->xget_object(class_name.c_str(), true, oid.c_str(), obj, rlevel, cnames, values);
      }
      else {
        m_server->write_cursor()->xget_object(m_server->write_session(), class_name.c_str(), true, oid.c_str(), obj, rlevel, cnames, values);
      }
    },
    "RdbConfiguration::get",
    "xget_object"
  )

// Waiting for Igor to tell me hoe to check whether the object was retrieved or not.
//	if (!obj) return false;

	// Leaving the object in the cache, since we might get it for use later after testing its existence.
  fill_cache(values);
  return true;
}


void
RdbConfiguration::get(const std::string& class_name, const std::string& oid, ConfigObject& object, unsigned long rlevel, const std::vector<std::string> * rclasses)
{
  if (rlevel && m_prefeteched_data == true)
    {
      ERS_DEBUG(3, "unset rlevel = " << rlevel << " for object \"" << oid << '@' << class_name << "\" because all data were prefetched");

      rlevel = 0;
      rclasses = nullptr;
    }

  const bool force_fill_cache = (rlevel && m_xcalls.insert(obj_ref_to_str(class_name, oid, rlevel, rclasses)).second == true);

  // try to find a valid object in cache
  if (ConfigObjectImpl * p = get_impl_object(class_name, oid))
    {
      if (p->is_deleted() == false)
        {
          object = static_cast<RdbConfigObject *>(p);

          if (force_fill_cache == false)
            return;
        }
      else
        {
          throw daq::config::NotFound( ERS_HERE, "object", oid.c_str() );
        }
    }
  else
    object = nullptr;

  // read object from rdb server
  rdb::RDBObject_var obj;
  rdb::RDBClassValueList_var values;
  rdb::RDBNameList cnames; vector2name_list(rclasses, cnames);

  m_number_of_get_object_calls++;

  ERS_DEBUG(2, "call implementation method xget_object(" << oid << '@' << class_name << param2str(rlevel, rclasses) << ')');

  RDB_SERVER_METHOD (
    {
      if(!m_server->is_writer()) {
        m_server->read_cursor()->xget_object(class_name.c_str(), true, oid.c_str(), obj, rlevel, cnames, values);
      }
      else {
        m_server->write_cursor()->xget_object(m_server->write_session(), class_name.c_str(), true, oid.c_str(), obj, rlevel, cnames, values);
      }
    },
    "RdbConfiguration::get",
    "xget_object"
  )

  fill_cache(values);

  if (object.is_null())
    object = new_object(obj);
}

void
RdbConfiguration::get(const std::string& class_name, std::vector<ConfigObject>& objects, const std::string& query, unsigned long rlevel, const std::vector<std::string> * rclasses)
{
  if (rlevel && m_xcalls.insert(query_ref_to_str(class_name, query, rlevel, rclasses)).second == false)
    {
      ERS_DEBUG(3, "unset rlevel = " << rlevel << " since query \"" << query << "\" was already executed for class \"" << class_name << '\"');

      rlevel = 0;
      rclasses = nullptr;
    }

  rdb::RDBObjectList_var olist;
  rdb::RDBClassValueList_var values;
  rdb::RDBNameList cnames; vector2name_list(rclasses, cnames);

  if(query.empty()) {
    if(m_prefeteched_data == false)
      {
        m_number_of_xget_all_objects_calls++;

        ERS_DEBUG(2, "call implementation method xget_all_objects(" << class_name << param2str(rlevel, rclasses) << ')');

        RDB_SERVER_METHOD (
        {
          if(!m_server->is_writer()) {
            m_server->read_cursor()->xget_all_objects(class_name.c_str(), true, olist, rlevel, cnames, values);
          }
          else {
            m_server->write_cursor()->xget_all_objects(m_server->write_session(), class_name.c_str(), true, olist, rlevel, cnames, values);
          }
        },
        "RdbConfiguration::get",
        "xget_all_objects"
        )
      }
    else
      {
        m_number_of_get_all_objects_calls++;

        ERS_DEBUG(2, m_number_of_get_all_objects_calls << " call implementation method get_all_objects(" << class_name << ')');

        RDB_SERVER_METHOD (
          {
              if (!m_server->is_writer())
                {
                  m_server->read_cursor()->get_all_objects(class_name.c_str(), true, olist);
                }
              else
                {
                  m_server->write_cursor()->get_all_objects(m_server->write_session(), class_name.c_str(), true, olist);
                }
          },
          "RdbConfiguration::get",
          "get_all_objects"
        )
      }
  }
  else
    {
      if(m_prefeteched_data == false)
        {
          m_number_of_xget_objects_by_query_calls++;

          ERS_DEBUG(2, "call implementation method xget_objects_by_query(" << class_name << ", query = \'" << query << '\'' << param2str(rlevel, rclasses) << ')');

          RDB_SERVER_METHOD (
            {
              if (!m_server->is_writer())
                {
                  m_server->read_cursor()->xget_objects_by_query(class_name.c_str(), query.c_str(), olist, rlevel, cnames, values);
                }
              else
                {
                  m_server->write_cursor()->xget_objects_by_query(m_server->write_session(), class_name.c_str(), query.c_str(), olist, rlevel, cnames, values);
                }
            },
            "RdbConfiguration::get",
            "xget_objects_by_query"
          )
        }
      else
        {
          m_number_of_get_objects_by_query_calls++;

          ERS_DEBUG(2, "call implementation method get_objects_by_query(" << class_name << ", query = \'" << query << '\'' << ')');

          RDB_SERVER_METHOD (
            {
              if (!m_server->is_writer())
                {
                  m_server->read_cursor()->get_objects_by_query(class_name.c_str(), query.c_str(), olist);
                }
              else
                {
                  m_server->write_cursor()->get_objects_by_query(m_server->write_session(), class_name.c_str(), query.c_str(), olist);
                }
            },
            "RdbConfiguration::get",
            "get_objects_by_query"
          )
        }
    }

  if (m_prefeteched_data == false)
    fill_cache(values);

  for (unsigned int idx = 0; idx < olist->length(); ++idx)
    {
      objects.push_back(new_object(olist[idx]));
    }
}

void
RdbConfiguration::get(const ConfigObject& obj_from, const std::string& query, std::vector<ConfigObject>& objects, unsigned long rlevel, const std::vector<std::string> * rclasses)
{
  rdb::RDBObject o;
  o.classid = obj_from.class_name().c_str();
  o.name = obj_from.UID().c_str();
  rdb::RDBObjectList_var olist;

  m_number_of_get_objects_by_path_calls++;

  ERS_DEBUG(2, "call implementation method get_objects_by_path(" << obj_from.UID() << '@' << obj_from.class_name() << ", path-query = \'" << query << '\'' << param2str(rlevel, rclasses) << ')');

  RDB_SERVER_METHOD (
    {
      if(!m_server->is_writer()) {
        m_server->read_cursor()->get_objects_by_path(o, query.c_str(), olist);
      }
      else {
        m_server->write_cursor()->get_objects_by_path(m_server->write_session(), o, query.c_str(), olist);
      }
    },
    "RdbConfiguration::get",
    "get_objects_by_path"
  )

  for(unsigned int idx = 0; idx < olist->length(); ++idx) {
    objects.push_back(new_object(olist[idx]));
  }
}


daq::config::class_t *
RdbConfiguration::get(const std::string& class_name, bool direct_only)
{
  m_number_of_get_meta_data_info_calls += 5;

  daq::config::class_t * d = 0;

    // get class info
  {
    rdb::RDBClass_var cv;

    ERS_DEBUG(2, "call implementation method get_class(" << class_name << ')' );

    RDB_SERVER_METHOD (
      {
        if(!m_server->is_writer()) {
          m_server->read_cursor()->get_class(class_name.c_str(), cv);
        }
        else {
          m_server->write_cursor()->get_class(m_server->write_session(), class_name.c_str(), cv);
        }
      },
      "RdbConfiguration::get",
      "get_class"
    )

    const rdb::RDBClass& c(cv);

    d = new daq::config::class_t(static_cast<const char *>(c.name), static_cast<const char *>(c.description), c.isAbstract);
  }
  
    // get superclasses
  {
    rdb::RDBClassList_var l;

    if(direct_only) {
      ERS_DEBUG(2, "call implementation method get_direct_super_classes(" << class_name << ')' );

      RDB_SERVER_METHOD (
        {
          if(!m_server->is_writer()) {
            m_server->read_cursor()->get_direct_super_classes(class_name.c_str(), l);
          }
          else {
            m_server->write_cursor()->get_direct_super_classes(m_server->write_session(), class_name.c_str(), l);
          }
        },
        "RdbConfiguration::get",
        "get_direct_super_classes"
      )
    }
    else {
      ERS_DEBUG(2, "call implementation method get_all_super_classes(" << class_name << ')' );

      RDB_SERVER_METHOD (
        {
          if(!m_server->is_writer()) {
            m_server->read_cursor()->get_all_super_classes(class_name.c_str(), l);
          }
          else {
            m_server->write_cursor()->get_all_super_classes(m_server->write_session(), class_name.c_str(), l);
          }
        },
	"RdbConfiguration::get",
	"get_all_super_classes"
      )
    }

    for(unsigned int i = 0; i < l->length(); ++i) {
      const_cast< std::vector<std::string>& >(d->p_superclasses).push_back(static_cast<const char *>(l[i].name));
    }  
  }


    // get subclasses
  {
    rdb::RDBClassList_var l;

    if(direct_only) {
      ERS_DEBUG(2, "call implementation method get_direct_sub_classes(" << class_name << ')' );

      RDB_SERVER_METHOD (
        {
          if(!m_server->is_writer()) {
            m_server->read_cursor()->get_direct_sub_classes(class_name.c_str(), l);
          }
          else {
            m_server->write_cursor()->get_direct_sub_classes(m_server->write_session(), class_name.c_str(), l);
          }
        },
        "RdbConfiguration::get",
        "get_sub_classes"
      )
    }
    else {
      ERS_DEBUG(2, "call implementation method get_sub_classes(" << class_name << ')' );

      RDB_SERVER_METHOD (
        {
          if(!m_server->is_writer()) {
            m_server->read_cursor()->get_sub_classes(class_name.c_str(), l);
          }
          else {
            m_server->write_cursor()->get_sub_classes(m_server->write_session(), class_name.c_str(), l);
          }
        },
        "RdbConfiguration::get",
        "get_sub_classes"
      )
    }

    for(unsigned int i = 0; i < l->length(); ++i) {
      const_cast< std::vector<std::string>& >(d->p_subclasses).push_back(static_cast<const char *>(l[i].name));
    }
  }
  
    // get attributes
  {
    rdb::RDBAttributeList_var l;

    ERS_DEBUG(2, "call implementation method get_attributes(" << class_name << ')' );

    RDB_SERVER_METHOD (
      {
        if(!m_server->is_writer()) {
          m_server->read_cursor()->get_attributes(class_name.c_str(), l);
        }
        else {
          m_server->write_cursor()->get_attributes(m_server->write_session(), class_name.c_str(), l);
        }
      },
      "RdbConfiguration::get",
      "get_attributes"
    )

    add_attributes(d, l.in(), direct_only);

  }

    // get relationships
  {
    rdb::RDBRelationshipList_var l;

    ERS_DEBUG(2, "call implementation method get_relationships(" << class_name << ')' );

    RDB_SERVER_METHOD (
      {
        if(!m_server->is_writer()) {
          m_server->read_cursor()->get_relationships(class_name.c_str(), l);
        }
        else {
          m_server->write_cursor()->get_relationships(m_server->write_session(), class_name.c_str(), l);
        }
      },
      "RdbConfiguration::get",
      "get_relationships"
    )

    add_relationships(d, l.in(), direct_only);
  }

  return d;
}

void
RdbConfiguration::get_superclasses(config::fmap<config::fset>& schema)
{
  schema.clear();

  if(m_server) {
    rdb::RDBInheritanceHierarchyList_var l = 0;

    m_number_of_get_meta_data_info_calls++;

    ERS_DEBUG(2, "call implementation method get_inheritance_hierarchy()" );

    RDB_SERVER_METHOD (
      {
        if(!m_server->is_writer()) {
          m_server->read_cursor()->get_inheritance_hierarchy(l);
        }
        else {
          m_server->write_cursor()->get_inheritance_hierarchy(m_server->write_session(), l);
        }
      },
      "RdbConfiguration::get_superclasses",
      "get_superclasses"
    )

    schema.reserve(l->length() * 3);

    for(unsigned int i = 0; i < l->length(); ++i) {
      const std::string* cname = &DalFactory::instance().get_known_class_name_ref(static_cast<const char *>(l[i].name));

      auto& subclasses = schema[cname];

      if(l[i].all_parents.length() > 0) {
        subclasses.reserve(l[i].all_parents.length() * 3);
        for(unsigned int j = 0; j < l[i].all_parents.length(); ++j) {
          subclasses.insert(&DalFactory::instance().get_known_class_name_ref(static_cast<const char *>(l[i].all_parents[j])));
        }
      }
    }

    l.out();
  }
}

void
RdbConfiguration::create(const std::string * at_file, const ConfigObject * at_obj, const std::string& class_name, const std::string& id, ConfigObject& object)
{
  if(!m_server->is_writer()) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfiguration::create", m_server->name()).c_str() ) );
  }

  if(at_file) {
    RDB_SERVER_METHOD (
      {
        m_server->write_cursor()->create_object(m_server->write_session(), class_name.c_str(), id.c_str(), at_file->c_str());
      },
      "RdbConfiguration::create",
      "create_object"
    )
  }
  else {
    rdb::RDBObject obj;
    obj.name = at_obj->UID().c_str();
    obj.classid = at_obj->class_name().c_str();
      
    RDB_SERVER_METHOD (
      {
        m_server->write_cursor()->new_object(m_server->write_session(), class_name.c_str(), id.c_str(), obj );
      },
      "RdbConfiguration::create",
      "new_object"
    )
  }

  rdb::RDBObject obj;
  obj.name = id.c_str();
  obj.classid = class_name.c_str();

  object = new_object(obj);

  clean_files_of_all_objects();
}

void
RdbConfiguration::destroy(::ConfigObject& obj)
{
  if(!m_server->is_writer()) {
    throw ( daq::config::Generic( ERS_HERE, rdbconfig::mk_wrong_server_exception_text("RdbConfiguration::destroy", m_server->name()).c_str() ) );
  }

  rdb::RDBClassChangesList_var rdb_changes; // removed objects , if some dependent children objects will be removed

  rdb::RDBObject o;
  o.name = obj.UID().c_str();
  o.classid = obj.class_name().c_str();

  RDB_SERVER_METHOD (
    {
      m_server->write_cursor()->delete_object(m_server->write_session(), o, rdb_changes);
    },
    "RdbConfiguration::destroy",
    "delete_object"
  )

  if(rdb_changes->length()) {
    std::vector<ConfigurationChange *> changes;

    for(unsigned int idx = 0; idx < rdb_changes->length(); idx++) {
      const char * name = static_cast<const char *>(rdb_changes[idx].name);
      for(unsigned int i3 = 0; i3 < rdb_changes[idx].removed.length(); i3++) {
        const char * id = static_cast<const char *>(rdb_changes[idx].removed[i3]);
	ConfigurationChange::add(changes, name, id, '-');
      }
    }

    if(!changes.empty()) {
      m_conf->update_cache(changes);
      ConfigurationChange::clear(changes);
    }
  }

  clean_files_of_all_objects();
}

bool
RdbConfiguration::loaded() const noexcept
{
  return (m_server && m_server->is_valid());
}

void
RdbConfiguration::subscribe(const std::set<std::string>& class_names,
                            const std::map< std::string , std::set<std::string> >& objs,
			    ConfigurationImpl::notify cb, ConfigurationImpl::pre_notify pre_cb)
{
  try {
    unsubscribe();
    m_fn = cb;
    m_pre_fn = pre_cb;
    m_receiver = new RDBInfoReceiver(*m_server->partition(), m_server->name().c_str(), m_server->write_session());
    m_receiver->subscribe(class_names, true, objs, inform, reinterpret_cast<int64_t>(this));
  }
  catch(ers::Issue & ex) {
    throw ( daq::config::Generic( ERS_HERE, "subscription failed", ex ) );
  }
}


void
RdbConfiguration::unsubscribe()
{
  if(m_receiver) {
    try {
      m_receiver->unsubscribe();
    }
    catch (ers::Issue & ex) {
      throw ( daq::config::Generic( ERS_HERE, "unsubscription failed", ex ) );
    }

    delete m_receiver;
    m_receiver = 0;
  }
}


  //
  // This structure is used to call user callback in a separate thread.
  // This allows RDB server do not block because of a long user code.
  //

struct RdbConfigurationNotify {
  std::vector<ConfigurationChange *> m_changes;
  RdbConfiguration * m_conf;

  RdbConfigurationNotify(RdbConfiguration * conf) : m_conf(conf) { ; }

  ~RdbConfigurationNotify() {
    ERS_DEBUG( 3 , "Call destructor of RdbConfigurationNotify object" );
    ConfigurationChange::clear(m_changes);
    m_conf = 0;
  }

  void operator()() {
    ERS_DEBUG( 2 , "Call user notification" );

    (*m_conf->m_fn)(m_changes, m_conf->m_conf);

    ERS_DEBUG( 4 , "Destroy RdbConfigurationNotify object = " << (void *)this );

    delete this;

    ERS_DEBUG( 2 , "Exit user notification" );
  }

};


void
RdbConfiguration::inform (RDBCallbackInfo * info) noexcept
{
  RdbConfiguration * conf = reinterpret_cast<RdbConfiguration *>(info->get_parameter());
  conf->m_prefeteched_data = false; // allow more efficient xget_all_objects method to refresh the data after modifications
  conf->m_xcalls.clear();

  ERS_DEBUG( 2 , "Enter RdbConfiguration::inform()");

  const rdb::RDBClassChangesList * rdb_changes = info->get_changes();

  if(rdb_changes && rdb_changes->length()) {
    (conf->m_pre_fn)(conf->m_conf);

    RdbConfigurationNotify * thread_obj = new RdbConfigurationNotify(conf);

    ERS_DEBUG( 4 , "Create RdbConfigurationNotify object = " << (void *)thread_obj );

    for(unsigned int idx = 0; idx < rdb_changes->length(); idx++) {
      const char * name = static_cast<const char *>((*rdb_changes)[idx].name);

      for(unsigned int i1 = 0; i1 < (*rdb_changes)[idx].modified.length(); i1++) {
	ConfigurationChange::add(
	  thread_obj->m_changes, name, static_cast<const char *>((*rdb_changes)[idx].modified[i1]), '~'
        );
      }

      for(unsigned int i2 = 0; i2 < (*rdb_changes)[idx].created.length(); i2++) {
	ConfigurationChange::add(
	  thread_obj->m_changes, name, static_cast<const char *>((*rdb_changes)[idx].created[i2]), '+'
        );
      }

      for(unsigned int i3 = 0; i3 < (*rdb_changes)[idx].removed.length(); i3++) {
	ConfigurationChange::add(
	  thread_obj->m_changes, name, static_cast<const char *>((*rdb_changes)[idx].removed[i3]), '-'
        );
      }
    }

    ERS_DEBUG( 3 , "Changes are:\n" << thread_obj->m_changes );

    std::thread thrd(std::ref(*thread_obj));
    thrd.detach();
  }
  else {
    ERS_DEBUG( 2 , "PROBLEM: there are no changes!" );
  }

  ERS_DEBUG( 2 , "Exit RdbConfiguration::inform()");
}


void
RdbConfiguration::print_profiling_info() noexcept
{
  std::cout <<
    "RdbConfiguration profiler report:\n"
    "  number of xget_object() calls: " << m_number_of_get_object_calls << "\n"
    "  number of get_all_objects() calls: " << m_number_of_get_all_objects_calls << "\n"
    "  number of get_objects_by_query() calls: " << m_number_of_get_objects_by_query_calls << "\n"
    "  number of xget_all_objects() calls: " << m_number_of_xget_all_objects_calls << "\n"
    "  number of xget_objects_by_query() calls: " << m_number_of_xget_objects_by_query_calls << "\n"
    "  number of get_objects_by_path() calls: " << m_number_of_get_objects_by_path_calls << "\n"
    "  number of cget_object_values() calls: " << RdbConfigObject::m_number_of_cget_object_values_calls << "\n"
    "  number of xget_object_values() calls: " << RdbConfigObject::m_number_of_xget_object_values_calls << "\n"
    "  number of calls accessing meta-data info: " << m_number_of_get_meta_data_info_calls << std::endl;
}
