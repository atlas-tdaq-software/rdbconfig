package rdbconfig;

import java.util.List;

public class RdbConfigObject implements config.ConfigObjectImpl
  {
    private RdbConfigServer m_server;
    private rdb.RDBObject m_obj;
    private rdb.RDBAttributeValue[] m_savalues;
    private rdb.RDBAttributeValues[] m_mavalues;
    private rdb.RDBRelationshipValue[] m_rvalues;

    public void clean()
      {
        m_savalues = null;
        m_mavalues = null;
        m_rvalues = null;
      }

    public RdbConfigObject(rdb.RDBObject obj, RdbConfigServer server)
      {
        m_obj = obj;
        m_server = server;
        clean();
      }

    public String UID()
      {
        return m_obj.name;
      }

    public String class_name()
      {
        return m_obj.classid;
      }

    public config.ConfigurationImpl get_configuration_impl()
      {
        return m_server.m_db;
      }

    private void get_object_values() throws config.NotFoundException, config.SystemException
      {
        if (m_savalues == null)
          {
            rdb.RDBAttributeValueListHolder lsa = new rdb.RDBAttributeValueListHolder();
            rdb.RDBAttributeValuesListHolder lma = new rdb.RDBAttributeValuesListHolder();
            rdb.RDBRelationshipValueListHolder lr = new rdb.RDBRelationshipValueListHolder();

            try
              {
                if (m_server.is_writer() == false)
                  {
                    m_server.read_cursor().get_object_values(m_obj.classid, m_obj.name, lsa, lma, lr);
                  }
                else
                  {
                    m_server.write_cursor().get_object_values(m_server.session_id(), m_obj.classid, m_obj.name, lsa, lma, lr);
                  }
                m_savalues = lsa.value;
                m_mavalues = lma.value;
                m_rvalues = lr.value;
              }
            catch (final org.omg.CORBA.SystemException ex)
              {
                throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
              }
            catch (final rdb.SessionNotFound ex)
              {
                throw new config.SystemException(RdbException.session_not_found_msg(ex));
              }
            catch (final rdb.NotFound ex)
              {
                throw new config.NotFoundException(rdb.NotFoundHelper.id(), ex.issue);
              }
            catch (final ers.Issue ex)
              {
                throw new config.SystemException("cannot get cursor", ex);
              }
          }
      }

    private rdb.DataUnion get_attribute_value(String name) throws config.NotFoundException, config.SystemException
      {
        get_object_values();

        for (int i = 0; i < m_savalues.length; i++)
          if (name.equals(m_savalues[i].name))
            return m_savalues[i].data;

        throw new config.NotFoundException("can not find single-value attribute with name \'" + name + "\' for object \"" + m_obj.name + '@' + m_obj.classid + '\"');
      }

    private rdb.DataListUnion get_attribute_values(String name) throws config.NotFoundException, config.SystemException
      {
        get_object_values();

        for (int i = 0; i < m_mavalues.length; i++)
          if (name.equals(m_mavalues[i].name))
            return m_mavalues[i].data;

        throw new config.NotFoundException("can not find multi-value attribute with name \'" + name + "\' for object \"" + m_obj.name + '@' + m_obj.classid + '\"');
      }

    public boolean get_bool(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_value(name).du_bool();
      }

    public char get_char(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_value(name).du_s8_int();
      }

    public byte get_byte(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_value(name).du_u8_int();
      }

    public short get_short(String name) throws config.NotFoundException, config.SystemException
      {
        rdb.DataUnion d = get_attribute_value(name);
        switch (d.discriminator().value())
          {
          case rdb.PrimitiveKind._pk_s16_int:
            return d.du_s16_int();
          case rdb.PrimitiveKind._pk_u16_int:
            return d.du_u16_int();
          }
        throw new config.NotFoundException("can not find s16 or u16 single-value attribute with name \"" + name + "\" for object \"" + m_obj.name + '@' + m_obj.classid + '\"');
      }

    public int get_int(String name) throws config.NotFoundException, config.SystemException
      {
        rdb.DataUnion d = get_attribute_value(name);
        switch (d.discriminator().value())
          {
          case rdb.PrimitiveKind._pk_s32_int:
            return d.du_s32_int();
          case rdb.PrimitiveKind._pk_u32_int:
            return d.du_u32_int();
          }
        throw new config.NotFoundException("can not find s32 or u32 single-value attribute with name \"" + name + "\" for object \"" + m_obj.name + '@' + m_obj.classid + '\"');
      }

    public long get_long(String name) throws config.NotFoundException, config.SystemException
      {
        rdb.DataUnion d = get_attribute_value(name);
        switch (d.discriminator().value())
          {
          case rdb.PrimitiveKind._pk_s64_int:
            return d.du_s64_int();
          case rdb.PrimitiveKind._pk_u64_int:
            return d.du_u64_int();
          }
        throw new config.NotFoundException("can not find s64 or u64 single-value attribute with name \"" + name + "\" for object \"" + m_obj.name + '@' + m_obj.classid + '\"');
      }

    public float get_float(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_value(name).du_float();
      }

    public double get_double(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_value(name).du_double();
      }

    public String get_string(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_value(name).du_string();
      }

    public boolean[] get_bools(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_values(name).du_bools();
      }

    public char[] get_chars(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_values(name).du_s8_ints();
      }

    public byte[] get_bytes(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_values(name).du_u8_ints();
      }

    public short[] get_shorts(String name) throws config.NotFoundException, config.SystemException
      {
        rdb.DataListUnion d = get_attribute_values(name);
        if (d.discriminator() == rdb.PrimitiveKind.pk_u16_int)
            return d.du_u16_ints();
        else if (d.discriminator() == rdb.PrimitiveKind.pk_s16_int)
            return d.du_u16_ints();
        else
            throw new config.NotFoundException("can not find s16 or u16 multi-value attribute with name \"" + name + "\" for object \"" + m_obj.name + '@' + m_obj.classid + '\"');
      }

    public int[] get_ints(String name) throws config.NotFoundException, config.SystemException
      {
        rdb.DataListUnion d = get_attribute_values(name);
        if (d.discriminator() == rdb.PrimitiveKind.pk_u32_int)
          return d.du_u32_ints();
        else if (d.discriminator() == rdb.PrimitiveKind.pk_s32_int)
          return d.du_s32_ints();
        else
          throw new config.NotFoundException("can not find s32 or u32 multi-value attribute with name \"" + name + "\" for object \"" + m_obj.name + '@' + m_obj.classid + '\"');
      }

    public long[] get_longs(String name) throws config.NotFoundException, config.SystemException
      {
        rdb.DataListUnion d = get_attribute_values(name);
        if (d.discriminator() == rdb.PrimitiveKind.pk_s64_int)
          return d.du_s64_ints();
        else if (d.discriminator() == rdb.PrimitiveKind.pk_u64_int)
          return d.du_u64_ints();
        else
          throw new config.NotFoundException("can not find s64 or u64 multi-value attribute with name \"" + name + "\" for object \"" + m_obj.name + '@' + m_obj.classid + '\"');
      }

    public float[] get_floats(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_values(name).du_floats();
      }

    public double[] get_doubles(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_values(name).du_doubles();
      }

    public String[] get_strings(String name) throws config.NotFoundException, config.SystemException
      {
        return get_attribute_values(name).du_strings();
      }

    public config.ConfigObject get_object(String name) throws config.NotFoundException, config.SystemException
      {
        get_object_values();
        for (int i = 0; i < m_rvalues.length; i++)
          {
            if (name.equals(m_rvalues[i].name))
              {
                if (m_rvalues[i].isMultiValue == true)
                  {
                    throw new config.NotFoundException("object \"" + m_obj.name + '@' + m_obj.classid + "\" has no single-value relationship with name \"" + name + '\"');
                  }
                else
                  {
                    if (m_rvalues[i].data.length > 0)
                      return new config.ConfigObject(new RdbConfigObject(m_rvalues[i].data[0], m_server));
                    else
                      return null;
                  }
              }
          }

        throw new config.NotFoundException("can not find relationship with name \"" + name + "\" for object \"" + m_obj.name + '@' + m_obj.classid + '\"');
      }

    public config.ConfigObject[] get_objects(String name) throws config.NotFoundException, config.SystemException
      {
        get_object_values();
        for (int i = 0; i < m_rvalues.length; i++)
          {
            if (name.equals(m_rvalues[i].name))
              {
                if (m_rvalues[i].isMultiValue == false)
                  {
                    throw new config.NotFoundException("object \"" + m_obj.name + '@' + m_obj.classid + "\" has single-value relationship with name \"" + name + '\"');
                  }
                else
                  {
                    config.ConfigObject[] value = new config.ConfigObject[m_rvalues[i].data.length];

                    for (int j = 0; j < m_rvalues[i].data.length; j++)
                      value[j] = new config.ConfigObject(new RdbConfigObject(m_rvalues[i].data[j], m_server));

                    return value;
                  }
              }
          }

        throw new config.NotFoundException("can not find relationship \"" + name + "\" for object \"" + m_obj.name + '@' + m_obj.classid + '\"');
      }

    private void set_attribute_value(String name, rdb.DataUnion value, rdb.DataListUnion values) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        try
          {
            if (value != null)
              m_server.write_cursor().set_attribute_value(m_server.session_id(), m_obj, name, value);
            else
              m_server.write_cursor().set_attribute_values(m_server.session_id(), m_obj, name, values);
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.NotFound ex)
          {
            throw new config.NotFoundException(rdb.NotFoundHelper.id(), ex.issue);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public void set_bool(String name, boolean value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataUnion data = new rdb.DataUnion();
        data.du_bool(value);
        set_attribute_value(name, data, null);
      }

    public void set_char(String name, char value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataUnion data = new rdb.DataUnion();
        data.du_s8_int(value);
        set_attribute_value(name, data, null);
      }

    public void set_byte(String name, byte value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataUnion data = new rdb.DataUnion();
        data.du_u8_int(value);
        set_attribute_value(name, data, null);
      }

    public void set_short(String name, short value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataUnion data = new rdb.DataUnion();
        rdb.DataUnion d = get_attribute_value(name);

        if (d.discriminator().value() == rdb.PrimitiveKind._pk_s16_int)
          data.du_s16_int(value);
        else
          data.du_u16_int(value);

        set_attribute_value(name, data, null);
      }

    public void set_int(String name, int value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataUnion data = new rdb.DataUnion();
        rdb.DataUnion d = get_attribute_value(name);

        if (d.discriminator().value() == rdb.PrimitiveKind._pk_s32_int)
          data.du_s32_int(value);
        else
          data.du_u32_int(value);

        set_attribute_value(name, data, null);
      }

    public void set_long(String name, long value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataUnion data = new rdb.DataUnion();
        rdb.DataUnion d = get_attribute_value(name);

        if (d.discriminator().value() == rdb.PrimitiveKind._pk_s64_int)
          data.du_s64_int(value);
        else
          data.du_u64_int(value);

        set_attribute_value(name, data, null);
      }

    public void set_float(String name, float value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataUnion data = new rdb.DataUnion();
        data.du_float(value);
        set_attribute_value(name, data, null);
      }

    public void set_double(String name, double value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataUnion data = new rdb.DataUnion();
        data.du_double(value);
        set_attribute_value(name, data, null);
      }

    public void set_string(String name, String value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataUnion data = new rdb.DataUnion();
        data.du_string(value);
        set_attribute_value(name, data, null);
      }

    public void set_bools(String name, boolean[] value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataListUnion data = new rdb.DataListUnion();
        data.du_bools(value);
        set_attribute_value(name, null, data);
      }

    public void set_chars(String name, char[] value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataListUnion data = new rdb.DataListUnion();
        data.du_s8_ints(value);
        set_attribute_value(name, null, data);
      }

    public void set_bytes(String name, byte[] value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataListUnion data = new rdb.DataListUnion();
        data.du_u8_ints(value);
        set_attribute_value(name, null, data);
      }

    public void set_shorts(String name, short[] value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataListUnion data = new rdb.DataListUnion();
        rdb.DataUnion d = get_attribute_value(name);

        if (d.discriminator().value() == rdb.PrimitiveKind._pk_s16_int)
          data.du_s16_ints(value);
        else
          data.du_u16_ints(value);

        set_attribute_value(name, null, data);
      }

    public void set_ints(String name, int[] value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataListUnion data = new rdb.DataListUnion();
        rdb.DataUnion d = get_attribute_value(name);

        if (d.discriminator().value() == rdb.PrimitiveKind._pk_s32_int)
          data.du_s32_ints(value);
        else
          data.du_u32_ints(value);

        set_attribute_value(name, null, data);
      }

    public void set_longs(String name, long[] value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataListUnion data = new rdb.DataListUnion();
        rdb.DataUnion d = get_attribute_value(name);

        if (d.discriminator().value() == rdb.PrimitiveKind._pk_s64_int)
          data.du_s64_ints(value);
        else
          data.du_u64_ints(value);

        set_attribute_value(name, null, data);
      }

    public void set_floats(String name, float[] value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataListUnion data = new rdb.DataListUnion();
        data.du_floats(value);
        set_attribute_value(name, null, data);
      }

    public void set_doubles(String name, double[] value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataListUnion data = new rdb.DataListUnion();
        data.du_doubles(value);
        set_attribute_value(name, null, data);
      }

    public void set_strings(String name, String[] value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        rdb.DataListUnion data = new rdb.DataListUnion();
        data.du_strings(value);
        set_attribute_value(name, null, data);
      }

    private void set_relationship(String name, config.ConfigObject value, config.ConfigObject[] values) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        // detect number of objects to be put into relationship and create array of RDB objects
        rdb.RDBObject[] v = new rdb.RDBObject[(values != null) ? values.length : 1];

        if (values == null)
          {
            v[0] = new rdb.RDBObject(value.UID(), value.class_name());
          }
        else
          {
            for (int i = 0; i < v.length; ++i)
              v[i] = new rdb.RDBObject(values[i].UID(), values[i].class_name());
          }

        try
          {
            m_server.write_cursor().set_objects_of_relationship(m_server.session_id(), m_obj, name, v, false);
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final rdb.NotFound ex)
          {
            throw new config.NotFoundException(rdb.NotFoundHelper.id(), ex.issue);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public void set_object(String name, config.ConfigObject value) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        set_relationship(name, value, null);
      }

    public void set_objects(String name, config.ConfigObject[] values) throws config.NotFoundException, config.NotAllowedException, config.SystemException
      {
        set_relationship(name, null, values);
      }

    public void referenced_by(List<config.ConfigObject> value, String relationship_name, boolean check_composite_only) throws config.SystemException
      {
        rdb.RDBObjectListHolder objs_h = new rdb.RDBObjectListHolder();

        try
          {
            if (m_server.is_writer() == false)
              m_server.read_cursor().get_referenced_by(m_obj, relationship_name, check_composite_only, objs_h);
            else
              m_server.write_cursor().get_referenced_by(m_server.session_id(), m_obj, relationship_name, check_composite_only, objs_h);

            for (int i = 0; i < objs_h.value.length; ++i)
              value.add(new config.ConfigObject(new RdbConfigObject(objs_h.value[i], m_server)));
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final rdb.NotFound ex)
          {
            throw new config.SystemException(rdb.NotFoundHelper.id(), ex.issue);
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }
  }
