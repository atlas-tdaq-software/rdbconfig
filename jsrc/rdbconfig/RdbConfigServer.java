package rdbconfig;

public class RdbConfigServer
  {

    private ipc.Partition m_partition;
    private String m_partition_name;
    private String m_server_name;
    private boolean m_inited;
    private boolean m_is_writer;
    private long m_write_session_id;
    private rdb.WriterPing m_cb;
    RdbConfiguration m_db;

    RdbConfigServer(ipc.Partition p, String p_name, String s_name, RdbConfiguration db)
      {
        m_partition = p;
        m_partition_name = p_name;
        m_server_name = s_name;
        m_inited = false;
        m_write_session_id = 0;
        m_cb = null;
        m_db = db;
      }

    public String server_name()
      {
        return m_server_name;
      }

    public long session_id()
      {
        return m_write_session_id;
      }

    public boolean is_writer() throws config.SystemException
      {
        init();
        return m_is_writer;
      }

    private void init() throws config.SystemException
      {
        if (m_inited == false)
          {
            rdb.cursor read_cursor = null;
            rdb.writer write_cursor = null;

            try
              {
                try
                  {
                    read_cursor = (rdb.cursor) m_partition.lookup(rdb.cursor.class, m_server_name);
                  }
                catch (ers.Issue ex)
                  {
                    write_cursor = (rdb.writer) m_partition.lookup(rdb.writer.class, m_server_name);

                    m_cb = new rdb.WriterPing();
                    m_write_session_id = write_cursor.open_session(rdb.CreateProcessInfo.get(null), m_cb._this(ipc.Core.getORB()));
                    m_cb.set_session_id(m_write_session_id);
                  }
              }
            catch (final org.omg.CORBA.SystemException ex)
              {
                throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
              }
            catch (final daq.tokens.AcquireTokenException ex)
              {
                throw new config.SystemException(RdbException.failed_acquire_daq_token_msg(), ex);
              }
            catch (final rdb.CannotProceed ex)
              {
                throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
              }
            catch (final ers.Issue ex)
              {
                throw new config.SystemException(mk_lookup_error_text(), ex);
              }

            if (read_cursor != null)
              m_is_writer = false;
            else if (write_cursor != null)
              m_is_writer = true;
            else
              throw new config.SystemException(mk_lookup_error_text() + ": lookup() failed");

            m_inited = true;
          }
      }

    public void close() throws config.SystemException
      {
        if (m_inited == true && is_writer())
          try
            {
              write_cursor().close_session(m_write_session_id, rdb.CreateProcessInfo.get(null));
            }
          catch (final org.omg.CORBA.SystemException ex)
            {
              throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
            }
          catch (final rdb.SessionNotFound ex)
            {
              throw new config.SystemException(RdbException.session_not_found_msg(ex));
            }
          catch (final daq.tokens.AcquireTokenException ex)
            {
              throw new config.SystemException(RdbException.failed_acquire_daq_token_msg(), ex);
            }
        }

    rdb.cursor read_cursor() throws config.SystemException
      {
        rdb.cursor cursor = null;

        try
          {
            cursor = (rdb.cursor) m_partition.lookup(rdb.cursor.class, m_server_name);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException(mk_lookup_error_text(), ex);
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }

        if (cursor == null)
          throw new config.SystemException(mk_lookup_error_text() + ": lookup() failed");

        return cursor;
      }

    rdb.writer write_cursor() throws config.SystemException
      {
        rdb.writer cursor = null;

        try
          {
            cursor = (rdb.writer) m_partition.lookup(rdb.writer.class, m_server_name);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException(mk_lookup_error_text(), ex);
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }

        if (cursor == null)
          throw new config.SystemException(mk_lookup_error_text() + ": lookup() failed");

        return cursor;
      }

    private String mk_lookup_error_text()
      {
        String text = "can not connect with RDB server \'" + m_server_name + "\' ";

        if (m_partition_name == null || m_partition_name.length() == 0)
          text.concat("in initial partition");
        else
          text.concat("in partition \'" + m_partition_name + "\'");

        return text;
      }
  }
