package rdbconfig;

import java.lang.System;

import java.util.TreeSet;
import java.util.TreeMap;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Iterator;

public class RdbConfiguration implements config.ConfigurationImpl
  {
    static boolean m_debug = Boolean.getBoolean("tdaq.config.debug");

    private boolean m_loaded;
    private RdbConfigServer m_server;
    private Callback m_cb;
    private int m_subscription_id;
    private config.Configuration m_db;

    public RdbConfiguration()
      {
        m_loaded = false;
        m_subscription_id = 0;
      }

    private void get_db(String db_name) throws config.SystemException
      {
        if (db_name == null || db_name.length() == 0)
          return;

        String server_name = new String();
        String part_name = new String();

        m_loaded = false;

        int idx = db_name.indexOf("::");
        int idx2 = db_name.indexOf("@");

        if (idx != -1)
          {
            part_name = db_name.substring(0, idx);
            server_name = db_name.substring(idx + 2);
          }
        else if (idx2 != -1)
          {
            server_name = db_name.substring(0, idx2);
            part_name = db_name.substring(idx2 + 1);
          }
        else
          {
            server_name = db_name;
            part_name = System.getProperty("tdaq.ipc.partition.name", "");

            if (part_name.length() == 0)
              {
                try
                  {
                    part_name = System.getenv("TDAQ_PARTITION");
                  }
                catch (final SecurityException e)
                  {
                    throw new config.SystemException("System.getenv() failed", e);
                  }
              }
          }

        ipc.Partition p = null;

        if (part_name == null || part_name.length() == 0)
          p = new ipc.Partition();
        else
          p = new ipc.Partition(part_name);

        m_server = new rdbconfig.RdbConfigServer(p, part_name, server_name, this);

        // test server existence, throw exception on error
        m_server.is_writer();

        m_loaded = true;
      }

    public void open_db(String db_name) throws config.SystemException
      {
        get_db(db_name);
      }

    public void close_db() throws config.SystemException
      {
        unsubscribe();
        m_server.close();
        m_server = null;
        m_loaded = false;
      }

    public boolean loaded()
      {
        return m_loaded;
      }

    public void set_configuration(config.Configuration db)
      {
        m_db = db;
      }

    public config.Configuration get_configuration()
      {
        return m_db;
      }

    public TreeMap<String, TreeSet<String>> get_superclasses() throws config.SystemException
      {
        rdb.RDBInheritanceHierarchyListHolder l = new rdb.RDBInheritanceHierarchyListHolder();

        try
          {
            if (m_server.is_writer() == false)
              m_server.read_cursor().get_inheritance_hierarchy(l);
            else
              m_server.write_cursor().get_inheritance_hierarchy(m_server.session_id(), l);

            TreeMap<String, TreeSet<String>> result = new TreeMap<String, TreeSet<String>>();

            for (int i = 0; i < l.value.length; i++)
              {
                if (l.value[i].all_parents != null)
                  {
                    TreeSet<String> set = new TreeSet<String>();

                    for (int j = 0; j < l.value[i].all_parents.length; j++)
                      {
                        set.add(l.value[i].all_parents[j]);
                      }

                    result.put(l.value[i].name, set);
                  }
              }

            return result;
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public void create(String db_name, String[] includes) throws config.SystemException, config.NotAllowedException, config.AlreadyExistsException
      {

        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        try
          {
            m_server.write_cursor().create_database(m_server.session_id(), db_name);
            if (includes != null)
              for (int i = 0; i < includes.length; ++i)
                m_server.write_cursor().database_add_file(m_server.session_id(), db_name, includes[i]);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }

      }

    public void create(String server_name, String db_name, String[] includes) throws config.SystemException, config.NotAllowedException, config.AlreadyExistsException
      {
        get_db(server_name);
        create(db_name, includes);
      }

    public void add_include(String db_name, String include) throws config.SystemException, config.NotFoundException, config.NotAllowedException
      {

        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        try
          {
            m_server.write_cursor().database_add_file(m_server.session_id(), db_name, include);
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public void remove_include(String db_name, String include) throws config.SystemException, config.NotFoundException, config.NotAllowedException
      {

        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        try
          {
            rdb.RDBClassChangesListHolder changes = new rdb.RDBClassChangesListHolder(); // FIXME: update cache!!!
            m_server.write_cursor().database_remove_file(m_server.session_id(), db_name, include, changes);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public String[] get_includes(String db_name) throws config.SystemException, config.NotFoundException
      {

        try
          {
            rdb.RDBNameListHolder ilist = new rdb.RDBNameListHolder();

            if (m_server.is_writer() == false)
              m_server.read_cursor().database_get_files(db_name, ilist);
            else
              m_server.write_cursor().database_get_files(m_server.session_id(), db_name, ilist);

            String[] includes = new String[ilist.value.length];

            for (int i = 0; i < ilist.value.length; i++)
              includes[i] = new String(ilist.value[i]);

            return includes;
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public String[] get_updated_dbs() throws config.SystemException, config.NotAllowedException
      {

        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        try
          {
            rdb.RDBNameListHolder dbs_list = new rdb.RDBNameListHolder();
            rdb.RDBNameListHolder dummu1 = new rdb.RDBNameListHolder();
            rdb.RDBNameListHolder dummu2 = new rdb.RDBNameListHolder();

            m_server.write_cursor().get_modified_databases(m_server.session_id(), dbs_list, dummu1, dummu2);

            String[] dbs = new String[dbs_list.value.length];

            for (int i = 0; i < dbs_list.value.length; i++)
              dbs[i] = new String(dbs_list.value[i]);

            return dbs;
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public void set_commit_credentials(String user, String password) throws config.SystemException, config.NotAllowedException
      {

        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        try
          {
            m_server.write_cursor().set_commit_credentials(m_server.session_id(), user, password);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public boolean commit(String log_message) throws config.SystemException, config.NotAllowedException
      {

        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        String token = "";

        if (daq.tokens.JWToken.enabled())
          try
            {
              token = daq.tokens.JWToken.acquire(daq.tokens.JWToken.MODE.REUSE);
            }
          catch (daq.tokens.AcquireTokenException ex)
            {
              throw new config.SystemException(RdbException.failed_acquire_daq_token_msg(), ex);
            }
        else
          ers.Logger.debug(1, new ers.Issue("daq.tokens are disabled"));

        try
          {
            m_server.write_cursor().commit_changes(m_server.session_id(), token, log_message);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }

        return true;
      }

    public boolean abort() throws config.SystemException, config.NotAllowedException
      {

        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        try
          {
            m_server.write_cursor().abort_changes(m_server.session_id());
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }

        return true;
      }

    public config.ConfigObject get(String class_name, String object_id) throws config.SystemException, config.NotFoundException
      {

        try
          {
            rdb.RDBObjectHolder rdb_obj = new rdb.RDBObjectHolder();
            if (m_server.is_writer() == false)
              m_server.read_cursor().get_object(class_name, true, object_id, rdb_obj);
            else
              m_server.write_cursor().get_object(m_server.session_id(), class_name, true, object_id, rdb_obj);
            return new config.ConfigObject(new RdbConfigObject(rdb_obj.value, m_server));
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.NotFound ex)
          {
            throw new config.NotFoundException(rdb.NotFoundHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public config.ConfigObject[] get(String class_name, config.Query query) throws config.SystemException, config.NotFoundException
      {

        try
          {
            rdb.RDBObjectListHolder rdb_objs = new rdb.RDBObjectListHolder();
            if (query == null || query.get_query_string().length() == 0)
              {
                if (m_server.is_writer() == false)
                  m_server.read_cursor().get_all_objects(class_name, true, rdb_objs);
                else
                  m_server.write_cursor().get_all_objects(m_server.session_id(), class_name, true, rdb_objs);
              }
            else
              {
                if (m_server.is_writer() == false)
                  m_server.read_cursor().get_objects_by_query(class_name, query.get_query_string(), rdb_objs);
                else
                  m_server.write_cursor().get_objects_by_query(m_server.session_id(), class_name, query.get_query_string(), rdb_objs);
              }

            config.ConfigObject[] objs = new config.ConfigObject[rdb_objs.value.length];

            for (int i = 0; i < rdb_objs.value.length; i++)
              objs[i] = new config.ConfigObject(new RdbConfigObject(rdb_objs.value[i], m_server));

            return objs;
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.NotFound ex)
          {
            throw new config.NotFoundException(rdb.NotFoundHelper.id(), ex.issue);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public config.ConfigObject[] get(config.ConfigObject from, config.Query query) throws config.SystemException, config.NotFoundException
      {

        try
          {
            rdb.RDBObjectListHolder rdb_objs = new rdb.RDBObjectListHolder();

            if (m_server.is_writer() == false)
              m_server.read_cursor().get_objects_by_path(new rdb.RDBObject(from.UID(), from.class_name()), query.get_query_string(), rdb_objs);
            else
              m_server.write_cursor().get_objects_by_path(m_server.session_id(), new rdb.RDBObject(from.UID(), from.class_name()), query.get_query_string(), rdb_objs);

            config.ConfigObject[] objs = new config.ConfigObject[rdb_objs.value.length];

            for (int i = 0; i < rdb_objs.value.length; i++)
              objs[i] = new config.ConfigObject(new RdbConfigObject(rdb_objs.value[i], m_server));

            return objs;
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.NotFound ex)
          {
            throw new config.NotFoundException(rdb.NotFoundHelper.id(), ex.issue);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public config.ConfigObject create(String db, config.ConfigObject at, String class_name, String object_id) throws config.SystemException, config.NotFoundException, config.NotAllowedException
      {

        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        try
          {
            if (db == null)
              m_server.write_cursor().new_object(m_server.session_id(), class_name, object_id, new rdb.RDBObject(at.UID(), at.class_name()));
            else
              m_server.write_cursor().create_object(m_server.session_id(), class_name, object_id, db);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.NotFound ex)
          {
            throw new config.NotFoundException(rdb.NotFoundHelper.id(), ex.issue);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }

        return new config.ConfigObject(new RdbConfigObject(new rdb.RDBObject(object_id, class_name), m_server));
      }

    public void destroy(config.ConfigObject object) throws config.SystemException, config.NotFoundException, config.NotAllowedException
      {

        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        try
          {
            rdb.RDBClassChangesListHolder changes = new rdb.RDBClassChangesListHolder(); // FIXME: update cache!!!
            m_server.write_cursor().delete_object(m_server.session_id(), new rdb.RDBObject(object.UID(), object.class_name()), changes);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final rdb.NotFound ex)
          {
            throw new config.NotFoundException(rdb.NotFoundHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public void move(config.ConfigObject o, String at) throws config.SystemException, config.NotFoundException, config.NotAllowedException
      {

        if (m_server.is_writer() == false)
          throw new config.NotAllowedException(RdbException.bad_server_op(m_server.server_name()));

        try
          {
            m_server.write_cursor().move_object(m_server.session_id(), new rdb.RDBObject(o.UID(), o.class_name()), at);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.NotFound ex)
          {
            throw new config.NotFoundException(rdb.NotFoundHelper.id(), ex.issue);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public void subscribe(TreeSet<String> classes, Hashtable<String, TreeSet<String>> objects, config.Configuration conf) throws config.SystemException
      {

        String[] cl = null;
        rdb.RDBObject[] ol = null;

        if (classes != null)
          {
            int j = 0;
            cl = new String[classes.size()];
            for (Iterator<String> i = classes.iterator(); i.hasNext(); ++j)
              {
                cl[j] = i.next();
              }
          }

        if (objects != null)
          {

            Enumeration<String> keys;

            // calculate total number of objects

            int count = 0;

            for (keys = objects.keys(); keys.hasMoreElements();)
              {
                String cl_name = (String) keys.nextElement();
                java.util.TreeSet<String> cl_objs = objects.get(cl_name);
                count += cl_objs.size();
              }

            if (count > 0)
              {
                ol = new rdb.RDBObject[count];
              }

            // iterate by all objects and fill array of rdb objects

            count = 0;

            for (keys = objects.keys(); keys.hasMoreElements();)
              {
                String cl_name = (String) keys.nextElement();
                java.util.TreeSet<String> cl_objs = objects.get(cl_name);

                for (Iterator<String> i = cl_objs.iterator(); i.hasNext();)
                  {
                    ol[count++] = new rdb.RDBObject(i.next(), cl_name);
                  }
              }
          }

        unsubscribe();

        try
          {
            m_cb = new Callback(conf, this);

            if (cl == null)
              cl = new String[0];

            if (ol == null)
              ol = new rdb.RDBObject[0];

            if (m_server.is_writer() == false)
              {
                m_subscription_id = m_server.read_cursor().subscribe(rdb.CreateProcessInfo.get(null), cl, true, ol, m_cb._this(ipc.Core.getORB()), 0);
              }
            else
              {
                m_server.write_cursor().subscribe(m_server.session_id(), rdb.CreateProcessInfo.get(null), cl, true, ol, m_cb._this(ipc.Core.getORB()), 0);
                m_subscription_id = 1;
              }
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
       catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public void unsubscribe() throws config.SystemException
      {
        if (m_subscription_id != 0)
          {
            try
              {
                if (m_server.is_writer() == false)
                  {
                    m_server.read_cursor().unsubscribe(rdb.CreateProcessInfo.get(null), m_subscription_id);
                  }
                else
                  {
                    m_server.write_cursor().unsubscribe(m_server.session_id(), rdb.CreateProcessInfo.get(null));
                  }
              }
            catch (final rdb.SessionNotFound ex)
              {
                ers.Logger.debug(0, new ers.Issue("ignore", new rdb.SessionNotFoundException(ex)));
              }
            catch (final org.omg.CORBA.SystemException ex)
              {
                ers.Logger.debug(0, new ers.Issue("ignore", new rdb.CorbaSystemException(ex)));
              }
            catch (final rdb.CannotProceed ex)
              {
                ers.Logger.debug(0, new ers.Issue("ignore", new rdb.CannotProceedException(ex)));
              }
            catch (final ers.Issue ex)
              {
                ers.Logger.debug(0, new ers.Issue("ignore", ex));
              }

            m_subscription_id = 0;
            m_cb = null;
          }
        else
          {
            ers.Logger.debug(1, new ers.Issue("no subscription"));
          }
      }

    class Callback extends rdb.callbackPOA
      {
        config.Configuration m_config;
        rdbconfig.RdbConfiguration m_impl;

        Callback(config.Configuration listener, rdbconfig.RdbConfiguration impl)
          {
            m_config = listener;
            m_impl = impl;
          }

        public final int inform(rdb.RDBClassChanges[] changes_list, long parameter)
          {
            long startTime = System.nanoTime();

            config.Change[] changes = new config.Change[changes_list.length];

            for (int i = 0; i < changes_list.length; ++i)
              changes[i] = new config.Change(changes_list[i].name, changes_list[i].created, changes_list[i].modified, changes_list[i].removed);

            config.NotifyThread p = new config.NotifyThread(m_config, changes);
            p.start();

            long endTime = System.nanoTime();
            long elapsedTime = (endTime - startTime) / 1000;
            return (int) elapsedTime;
          }

        public final void server_exits(long parameter)
          {
            if (m_debug)
              {
                System.out.println("DEBUG [rdbconfig.Callback.server_exits()]: method was called");
              }
            m_impl.m_subscription_id = 0;
            m_impl.m_cb = null;
          }
      }

    public config.class_t get(String class_name, boolean direct_only) throws config.SystemException, config.NotFoundException
      {
        rdb.RDBClassHolder class_h = new rdb.RDBClassHolder();
        rdb.RDBClassListHolder super_classes_h = new rdb.RDBClassListHolder();
        rdb.RDBClassListHolder sub_classes_h = new rdb.RDBClassListHolder();
        rdb.RDBAttributeListHolder attributes_h = new rdb.RDBAttributeListHolder();
        rdb.RDBRelationshipListHolder relationships_h = new rdb.RDBRelationshipListHolder();

        try
          {
            if (m_server.is_writer() == false)
              {
                m_server.read_cursor().get_class(class_name, class_h);

                if (direct_only)
                  {
                    m_server.read_cursor().get_direct_sub_classes(class_name, sub_classes_h);
                  }
                else
                  {
                    m_server.read_cursor().get_all_super_classes(class_name, super_classes_h);
                    m_server.read_cursor().get_sub_classes(class_name, sub_classes_h);
                  }

                m_server.read_cursor().get_attributes(class_name, attributes_h);
                m_server.read_cursor().get_relationships(class_name, relationships_h);
              }
            else
              {
                long session = m_server.session_id();

                m_server.write_cursor().get_class(session, class_name, class_h);

                if (direct_only)
                  {
                    m_server.write_cursor().get_direct_super_classes(session, class_name, super_classes_h);
                    m_server.write_cursor().get_direct_sub_classes(session, class_name, sub_classes_h);
                  }
                else
                  {
                    m_server.write_cursor().get_all_super_classes(session, class_name, super_classes_h);
                    m_server.write_cursor().get_sub_classes(session, class_name, sub_classes_h);
                  }

                m_server.write_cursor().get_attributes(session, class_name, attributes_h);
                m_server.write_cursor().get_relationships(session, class_name, relationships_h);
              }

            // process superclasses

            String[] superclasses = null;
            if (super_classes_h.value.length > 0)
              {
                superclasses = new String[super_classes_h.value.length];
                for (int i = 0; i < super_classes_h.value.length; ++i)
                  {
                    superclasses[i] = super_classes_h.value[i].name;
                  }
              }

            // process subclasses

            String[] subclasses = null;
            if (sub_classes_h.value.length > 0)
              {
                subclasses = new String[sub_classes_h.value.length];
                for (int i = 0; i < sub_classes_h.value.length; ++i)
                  {
                    subclasses[i] = sub_classes_h.value[i].name;
                  }
              }

            // process attributes

            config.attribute_t[] attributes = null;
            if (attributes_h.value.length > 0)
              {
                int len2 = 0;
                if (direct_only == true)
                  {
                    for (int i = 0; i < attributes_h.value.length; ++i)
                      {
                        if (attributes_h.value[i].isDirect == true)
                          len2++;
                      }
                  }
                else
                  {
                    len2 = attributes_h.value.length;
                  }

                if (len2 != 0)
                  {
                    attributes = new config.attribute_t[len2];
                    for (int i = 0, j = 0; i < attributes_h.value.length; ++i)
                      {
                        if (direct_only == true && attributes_h.value[i].isDirect == false)
                          continue;

                        String type = attributes_h.value[i].type;

                        attributes[j++] = new config.attribute_t(
                            attributes_h.value[i].name,
                              (
                                type.equals("string") ? config.attribute_t.type_t.string_type :
                                type.equals("enum") ? config.attribute_t.type_t.enum_type :
                                type.equals("bool") ? config.attribute_t.type_t.bool_type :
                                type.equals("s8") ? config.attribute_t.type_t.s8_type :
                                type.equals("u8") ? config.attribute_t.type_t.u8_type :
                                type.equals("s16") ? config.attribute_t.type_t.s16_type :
                                type.equals("u16") ? config.attribute_t.type_t.u16_type :
                                type.equals("s32") ? config.attribute_t.type_t.s32_type :
                                type.equals("u32") ? config.attribute_t.type_t.u32_type :
                                type.equals("s64") ? config.attribute_t.type_t.s64_type :
                                type.equals("u64") ? config.attribute_t.type_t.u64_type :
                                type.equals("float") ? config.attribute_t.type_t.float_type :
                                type.equals("double") ? config.attribute_t.type_t.double_type :
                                type.equals("date") ? config.attribute_t.type_t.date_type :
                                type.equals("time") ? config.attribute_t.type_t.time_type :
                                config.attribute_t.type_t.class_type
                              ),
                            attributes_h.value[i].range,
                              (
                                attributes_h.value[i].intFormat == rdb.IntegerFormat.int_format_na ? config.attribute_t.int_format_t.na_int_format :
                                attributes_h.value[i].intFormat == rdb.IntegerFormat.int_format_dec ? config.attribute_t.int_format_t.dec_int_format :
                                attributes_h.value[i].intFormat == rdb.IntegerFormat.int_format_hex ? config.attribute_t.int_format_t.hex_int_format :
                                config.attribute_t.int_format_t.oct_int_format
                              ),
                            attributes_h.value[i].isNotNull,
                            attributes_h.value[i].isMultiValue,
                            attributes_h.value[i].initValue,
                            attributes_h.value[i].description
                          );
                      }
                  }
              }

            // process relationships

            config.relationship_t[] relationships = null;
            if (relationships_h.value.length > 0)
              {
                int len2 = 0;
                if (direct_only == true)
                  {
                    for (int i = 0; i < relationships_h.value.length; ++i)
                      {
                        if (relationships_h.value[i].isDirect == true)
                          len2++;
                      }
                  }
                else
                  {
                    len2 = relationships_h.value.length;
                  }

                if (len2 != 0)
                  {
                    relationships = new config.relationship_t[len2];
                    for (int i = 0, j = 0; i < relationships_h.value.length; ++i)
                      {
                        if (direct_only == true && relationships_h.value[i].isDirect == false)
                          continue;

                        relationships[j++] = new config.relationship_t
                          (
                            relationships_h.value[i].name,
                            relationships_h.value[i].classid,
                            (relationships_h.value[i].isNotNull == false),
                            relationships_h.value[i].isMultiValue,
                            relationships_h.value[i].isAggregation,
                            relationships_h.value[i].description
                          );
                      }
                  }
              }

            return new config.class_t(class_h.value.name, class_h.value.description, class_h.value.isAbstract, superclasses, subclasses, attributes, relationships);
          }
        catch (final rdb.SessionNotFound ex)
          {
            throw new config.SystemException(RdbException.session_not_found_msg(ex));
          }
        catch (final rdb.NotFound ex)
          {
            throw new config.NotFoundException(rdb.NotFoundHelper.id(), ex.issue);
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    private final config.Version[] rdb2config(rdb.RDBRepositoryVersion[] val)
      {
        final config.Version[] versions = new config.Version[val.length];

        for (int i = 0; i < val.length; ++i)
          versions[i] = new config.Version(val[i].id, val[i].user, val[i].timestamp, val[i].comment, val[i].files);

        return versions;
      }

    public config.Version[] get_changes() throws config.SystemException
      {
        try
          {
            return rdb2config((m_server.is_writer() == false) ? m_server.read_cursor().get_changes() : m_server.write_cursor().get_changes());
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

    public config.Version[] get_versions(String since, String until, config.Version.QueryType type, boolean skip) throws config.SystemException
      {
        try
          {
            final rdb.VersionsQueryType t = (type == config.Version.QueryType.query_by_date ? rdb.VersionsQueryType.query_by_date : type == config.Version.QueryType.query_by_id ? rdb.VersionsQueryType.query_by_id : rdb.VersionsQueryType.query_by_tag);
            return rdb2config((m_server.is_writer() == false) ? m_server.read_cursor().get_versions(skip, t, since, until) : m_server.write_cursor().get_versions(skip, t, since, until));
          }
        catch (final org.omg.CORBA.SystemException ex)
          {
            throw new config.SystemException(RdbException.corba_system_exception_msg(), ex);
          }
        catch (final rdb.CannotProceed ex)
          {
            throw new config.SystemException(rdb.CannotProceedHelper.id(), ex.issue);
          }
        catch (final ers.Issue ex)
          {
            throw new config.SystemException("cannot get cursor", ex);
          }
      }

  }
