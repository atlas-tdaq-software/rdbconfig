package rdbconfig;

class RdbException
  {
    static public String bad_server_op(String name)
      {
        return "read-only server \'" + name + "\' cannot be used for update operation";
      }
    
    static public String session_not_found_msg(rdb.SessionNotFound ex)
      {
        return "remote method failed with " + rdb.SessionNotFoundHelper.id() + " exception: session" + ex.session_id + " is not found";
      }
    
    static public String failed_acquire_daq_token_msg()
      {
        return "failed acquire DAQ token";
      }

    static public String corba_system_exception_msg()
      {
        return "remote method failed with CORBA system exception";
      }
  }